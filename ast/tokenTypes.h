#ifndef _TOKEN_TYPES_H
#define _TOKEN_TYPES_H

#include "astBasic.h"
#include "expression.h"

#include <deque>
#include <string>

// ////////////////////////////////////////////////////////////////////////// //

class InstructionList : public AbstractAst {
private:
    std::deque<const Instruction*> instructions;
public:
    InstructionList();
    ~InstructionList();
    void visit(AstVisitor &visitor) const;
    void addInstruction(const Instruction *instruction);
    std::deque<const Instruction*>::const_iterator getInstructions() const;
    std::deque<const Instruction*>::const_iterator getInstructionsEnd() const;
    int getSize() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ParamsList : public AbstractAst {
private:
    std::deque<const Expression*> parametres;
public:
    ParamsList();
    ~ParamsList();
    void visit(AstVisitor &visitor) const;
    void addParam(const Expression *param);
    std::deque<const Expression*>::const_iterator getParametres() const;
    std::deque<const Expression*>::const_iterator getParametresEnd() const;
    int getSize() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ParamsProto : public AbstractAst {
private:
    const std::string type;
    const std::string name;
public:
    ParamsProto(std::string type, std::string name);
    void visit(AstVisitor &visitor) const;
    const std::string getType() const;
    const std::string getName() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ParamsProtoList : public AbstractAst {
private:
    std::deque<const ParamsProto*> parametres;
public:
    ParamsProtoList();
    ~ParamsProtoList();
    void visit(AstVisitor &visitor) const;
    void addParam(const ParamsProto *param);
    std::deque<const ParamsProto*>::const_iterator getParametres() const;
    std::deque<const ParamsProto*>::const_iterator getParametresEnd() const;
    int getSize() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ForParametres : public AbstractAst {
private:
    const InstructionList* init;
    const Expression* condition;
    const InstructionList* increment;
public:
    ForParametres(const InstructionList* init, const Expression* condition, const InstructionList *increment);
    ~ForParametres();
    void visit(AstVisitor &visitor) const;
    const InstructionList* getInit() const;
    const Expression* getCondition() const;
    const InstructionList* getIncrement() const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _TOKEN_TYPES_H
