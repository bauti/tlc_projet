#include "controle.h"

// ////////////////////////////////////////////////////////////////////////// //

ControleIf::ControleIf(const Expression* condition, const InstructionList* trueInstructions, const InstructionList* falseInstructions) :
        condition(condition), trueInstructions(trueInstructions), falseInstructions(falseInstructions) {

}

ControleIf::~ControleIf() {
    delete this->condition;
    delete this->trueInstructions;
    delete this->falseInstructions;
}

void ControleIf::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const Expression* ControleIf::getCondition() const {
    return this->condition;
}

const InstructionList* ControleIf::getTrueInstructions() const {
    return this->trueInstructions;
}

const InstructionList* ControleIf::getFalseInstructions() const {
    return this->falseInstructions;
}

// ////////////////////////////////////////////////////////////////////////// //

ControleFor::ControleFor(const ForParametres* parametres, const InstructionList* instructions) :
        parametres(parametres), instructions(instructions) {

}

ControleFor::~ControleFor() {
    delete this->parametres;
    delete this->instructions;
}

void ControleFor::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const ForParametres* ControleFor::getParametres() const {
    return this->parametres;
}

const InstructionList* ControleFor::getInstructions() const {
    return this->instructions;
}

// ////////////////////////////////////////////////////////////////////////// //

ControleWhile::ControleWhile(const Expression* condition, const InstructionList* instructions) :
        condition(condition), instructions(instructions) {

}

ControleWhile::~ControleWhile() {
    delete this->condition;
    delete this->instructions;
}

void ControleWhile::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const Expression* ControleWhile::getCondition() const {
    return this->condition;
}

const InstructionList* ControleWhile::getInstructions() const {
    return this->instructions;
}

// ////////////////////////////////////////////////////////////////////////// //

ControleReturn::ControleReturn(const Expression* result) :
        result(result) {

}

ControleReturn::~ControleReturn() {
    if(this->result != NULL) {
        delete this->result;
    }
}

void ControleReturn::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const Expression* ControleReturn::getResult() const {
    return this->result;
}

// ////////////////////////////////////////////////////////////////////////// //
