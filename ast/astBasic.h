#ifndef _AST_BASIC_H
#define _AST_BASIC_H

#include "visitor/astVisitor.h"

class AbstractAst {
public:
    AbstractAst();
    virtual ~AbstractAst();

    virtual void visit(AstVisitor &visitor) const = 0;
};

// ////////////////////////////////////////////////////////////////////////// //

class Instruction : public AbstractAst {
public:
    Instruction();
    virtual ~Instruction();
};


// ////////////////////////////////////////////////////////////////////////// //

class Expression : public Instruction {
public:
    Expression();
};

#endif // _AST_BASIC_H
