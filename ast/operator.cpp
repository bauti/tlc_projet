#include "operator.h"

using namespace std;

// ////////////////////////////////////////////////////////////////////////// //

OperatorBin::OperatorBin(const Expression* left, const Expression* right, const string operation) :
        left(left), right(right), operation(operation) {

}

OperatorBin::~OperatorBin() {
    delete this->left;
    delete this->right;
}

const Expression* OperatorBin::getLeft() const {
    return this->left;
}

const Expression* OperatorBin::getRight() const {
    return this->right;
}

const std::string OperatorBin::getOperation() const {
    return this->operation;
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorPlus::OperatorPlus(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "+") {

}

void OperatorPlus::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorMoins::OperatorMoins(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "-") {

}

void OperatorMoins::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorFois::OperatorFois(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "*") {

}

void OperatorFois::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorDiv::OperatorDiv(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "/") {

}

void OperatorDiv::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorMod::OperatorMod(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "%") {

}

void OperatorMod::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

OperatorPuiss::OperatorPuiss(const Expression* left, const Expression* right) :
        OperatorBin(left, right, "^") {

}

void OperatorPuiss::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //
