#ifndef _BOOL_H
#define _BOOL_H

#include "expression.h"
#include "tokenTypes.h"

class AstVisitor;

class ExpBooleen : public Expression {
public:
    ExpBooleen();
    virtual ~ExpBooleen();
};

// ////////////////////////////////////////////////////////////////////////// //

class BooleenComp : public ExpBooleen {
private:
    const Expression* left;
    const Expression* right;
public:
    BooleenComp(const Expression* left, const Expression* right);
    ~BooleenComp();
    const Expression* getLeft() const;
    const Expression* getRight() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class BoolEt : public BooleenComp {
private:
public:
    BoolEt(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class BoolOu : public BooleenComp {
public:
    BoolOu(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class BoolNon : public ExpBooleen {
private:
    const Expression* expr;
public:
    BoolNon(const Expression* expr);
    ~BoolNon();
    void visit(AstVisitor &visitor) const;
    const Expression* getExpr() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompEq : public BooleenComp {
public:
    CompEq(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompDf : public BooleenComp {
public:
    CompDf(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompPq : public BooleenComp {
public:
    CompPq(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompPe : public BooleenComp {
public:
    CompPe(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompGq : public BooleenComp {
public:
    CompGq(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class CompGe : public BooleenComp {
public:
    CompGe(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _BOOL_H
