#ifndef _CONTROLE_H
#define _CONTROLE_H

#include <cstdlib>
#include "tokenTypes.h"

// ////////////////////////////////////////////////////////////////////////// //

class ControleIf : public Instruction {
private:
    const Expression* condition;
    const InstructionList* trueInstructions;
    const InstructionList* falseInstructions;
public:
    ControleIf(const Expression* condition, const InstructionList* trueInstructions, const InstructionList* falseInstructions = NULL);
    ~ControleIf();
    void visit(AstVisitor &visitor) const;
    const Expression* getCondition() const;
    const InstructionList* getTrueInstructions() const;
    const InstructionList* getFalseInstructions() const;

};

// ////////////////////////////////////////////////////////////////////////// //

class ControleFor : public Instruction {
private:
    const ForParametres* parametres;
    const InstructionList* instructions;
public:
    ControleFor(const ForParametres* parametres, const InstructionList* instructions);
    ~ControleFor();
    void visit(AstVisitor &visitor) const;
    const ForParametres* getParametres() const;
    const InstructionList* getInstructions() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ControleWhile : public Instruction {
private:
    const Expression* condition;
    const InstructionList* instructions;
public:
    ControleWhile(const Expression* condition, const InstructionList* instructions);
    ~ControleWhile();
    void visit(AstVisitor &visitor) const;
    const Expression* getCondition() const;
    const InstructionList* getInstructions() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ControleReturn : public Instruction {
private:
    const Expression* result;
public:
    ControleReturn(const Expression* result = NULL);
    ~ControleReturn();
    void visit(AstVisitor &visitor) const;
    const Expression* getResult() const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _CONTROLE_H
