#include "tokenTypes.h"

#include <iostream>

using namespace std;

// ////////////////////////////////////////////////////////////////////////// //

InstructionList::InstructionList() :
        AbstractAst() {

}

InstructionList::~InstructionList() {
    deque<const Instruction*>::iterator it;
    for(it = this->instructions.begin(); it != this->instructions.end(); it++) {
        delete *it;
    }
}

void InstructionList::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

void InstructionList::addInstruction(const Instruction *instruction) {
    this->instructions.push_front(instruction);
}

deque<const Instruction*>::const_iterator InstructionList::getInstructions() const {
    return this->instructions.begin();
}

deque<const Instruction*>::const_iterator InstructionList::getInstructionsEnd() const {
    return this->instructions.end();
}

int InstructionList::getSize() const {
    return this->instructions.size();
}

// ////////////////////////////////////////////////////////////////////////// //

ParamsList::ParamsList() :
        AbstractAst() {

}

ParamsList::~ParamsList() {
    deque<const Expression*>::iterator it;
    for(it = this->parametres.begin(); it != this->parametres.end(); it++) {
        delete *it;
    }
}

void ParamsList::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

void ParamsList::addParam(const Expression *param) {
    this->parametres.push_front(param);
}

deque<const Expression*>::const_iterator ParamsList::getParametres() const {
    return this->parametres.begin();
}

deque<const Expression*>::const_iterator ParamsList::getParametresEnd() const {
    return this->parametres.end();
}

int ParamsList::getSize() const {
    return this->parametres.size();
}

// ////////////////////////////////////////////////////////////////////////// //

ParamsProto::ParamsProto(std::string type, std::string name) :
        type(type), name(name) {

}

void ParamsProto::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const std::string ParamsProto::getType() const {
    return this->type;
}

const std::string ParamsProto::getName() const {
    return this->name;
}

// ////////////////////////////////////////////////////////////////////////// //

ParamsProtoList::ParamsProtoList() :
        AbstractAst() {

}

ParamsProtoList::~ParamsProtoList() {
    deque<const ParamsProto*>::iterator it;
    for(it = this->parametres.begin(); it != this->parametres.end(); it++) {
        delete *it;
    }
}

void ParamsProtoList::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

void ParamsProtoList::addParam(const ParamsProto *param) {
    this->parametres.push_front(param);
}

deque<const ParamsProto*>::const_iterator ParamsProtoList::getParametres() const {
    return this->parametres.begin();
}

deque<const ParamsProto*>::const_iterator ParamsProtoList::getParametresEnd() const {
    return this->parametres.end();
}

int ParamsProtoList::getSize() const {
    return this->parametres.size();
}

// ////////////////////////////////////////////////////////////////////////// //

ForParametres::ForParametres(const InstructionList* init, const Expression* condition, const InstructionList *increment) :
        AbstractAst(), init(init), condition(condition), increment(increment) {

}

ForParametres::~ForParametres() {
    delete this->init;
    delete this->condition;
    delete this->increment;
}

void ForParametres::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}


const InstructionList* ForParametres::getInit() const {
        return this->init;
}

const Expression* ForParametres::getCondition() const {
    return this->condition;
}

const InstructionList* ForParametres::getIncrement() const {
    return this->increment;
}

// ////////////////////////////////////////////////////////////////////////// //
