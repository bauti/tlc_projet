#ifndef _PRIMITIVE_H
#define _PRIMITIVE_H

#include "tokenTypes.h"

// ////////////////////////////////////////////////////////////////////////// //

class Primitive : public Instruction {
private:
    const ParamsList* params;
public:
    Primitive(const ParamsList* params);
    ~Primitive();
    const ParamsList* getParams() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimAvance : public Primitive {
public:
    PrimAvance(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimRecule : public Primitive {
public:
    PrimRecule(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimTourneG : public Primitive {
public:
    PrimTourneG(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimTourneD : public Primitive {
public:
    PrimTourneD(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimLever : public Primitive {
public:
    PrimLever(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimBaisser : public Primitive {
public:
    PrimBaisser(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimSetCoul : public Primitive {
public:
    PrimSetCoul(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimSetPos : public Primitive {
public:
    PrimSetPos(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class PrimSetAngle : public Primitive {
public:
    PrimSetAngle(const ParamsList* params);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _PRIMITIVE_H
