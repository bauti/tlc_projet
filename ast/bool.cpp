#include "bool.h"
#include "expression.h"

// ////////////////////////////////////////////////////////////////////////// //

ExpBooleen::ExpBooleen() {

}

ExpBooleen::~ExpBooleen() {

}

// ////////////////////////////////////////////////////////////////////////// //

BooleenComp::BooleenComp(const Expression* left, const Expression* right) :
        ExpBooleen(), left(left), right(right) {

}

BooleenComp::~BooleenComp() {
    delete this->left;
    delete this->right;
}

const Expression* BooleenComp::getLeft() const {
    return this->left;
}

const Expression* BooleenComp::getRight() const {
    return this->right;
}

// ////////////////////////////////////////////////////////////////////////// //

BoolEt::BoolEt(const Expression* left, const Expression* right) :
        BooleenComp(left, right) {

}

void BoolEt::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

BoolOu::BoolOu(const Expression* left, const Expression* right) :
        BooleenComp(left, right) {

}

void BoolOu::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

BoolNon::BoolNon(const Expression* expr) :
        ExpBooleen(), expr(expr) {

}

BoolNon::~BoolNon() {
    delete this->expr;
}

void BoolNon::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const Expression* BoolNon::getExpr() const {
    return this->expr;
}

// ////////////////////////////////////////////////////////////////////////// //

CompEq::CompEq(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompEq::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

CompDf::CompDf(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompDf::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

CompPq::CompPq(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompPq::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

CompPe::CompPe(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompPe::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

CompGq::CompGq(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompGq::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

CompGe::CompGe(const Expression* left, const Expression* right) :
        BooleenComp(left,right) {

}

void CompGe::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //
