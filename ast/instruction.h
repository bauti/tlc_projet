#ifndef _INSTRUCTION_H
#define _INSTRUCTION_H

#include "astBasic.h"
#include "tokenTypes.h"

// ////////////////////////////////////////////////////////////////////////// //

class VariableCreate : public Instruction {
private:
    const char* type;
    const char* name;
public:
    VariableCreate(const char* type, const char* name);
    void visit(AstVisitor &visitor) const;
    const char* getType() const;
    const char* getName() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class Affect : public Instruction {
private:
    const char* name;
    const Expression* value;
public:
    Affect(const char* name, const Expression* value);
    ~Affect();
    void visit(AstVisitor &visitor) const;
    const char* getName() const;
    const Expression* getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class VariableCreateAffect : public Instruction {
private:
    const char* type;
    const char* name;
    const Expression* value;
public:
    VariableCreateAffect(const char* type, const char* name, const Expression* value);
    ~VariableCreateAffect();
    void visit(AstVisitor &visitor) const;
    const char* getType() const;
    const char* getName() const;
    const Expression* getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ObjectAttributeAffect : public Instruction {
private:
    const char* object;
    const char* attribute;
    const Expression* value;
public:
    ObjectAttributeAffect(const char* object, const char* attribute, const Expression* value);
    ~ObjectAttributeAffect();
    void visit(AstVisitor &visitor) const;
    const char* getObject() const;
    const char* getAttribute() const;
    const Expression* getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ObjectMethodCall : public Expression {
private:
    const char* object;
    const char* method;
    const ParamsList* parametres;
public:
    ObjectMethodCall(const char* object, const char* method, const ParamsList* params);
    ~ObjectMethodCall();
    void visit(AstVisitor &visitor) const;
    const char* getObject() const;
    const char* getMethod() const;
    const ParamsList* getParametres() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class MethodCall : public Expression {
private:
    const char* method;
    const ParamsList* parametres;
public:
    MethodCall(const char* method, const ParamsList* params);
    ~MethodCall();
    void visit(AstVisitor &visitor) const;
    const char* getMethod() const;
    const ParamsList* getParametres() const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _INSTRUCTION_H
