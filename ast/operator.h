#ifndef _OPERATOR_H
#define _OPERATOR_H

#include <string>

#include "tokenTypes.h"

// ////////////////////////////////////////////////////////////////////////// //

class OperatorBin : public Expression {
private:
    const Expression* left;
    const Expression* right;
    const std::string operation;
public:
    OperatorBin(const Expression* left, const Expression* right, const std::string operation);
    ~OperatorBin();
    const Expression* getLeft() const;
    const Expression* getRight() const;
    const std::string getOperation() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorPlus : public OperatorBin {
public:
    OperatorPlus(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorMoins : public OperatorBin {
public:
    OperatorMoins(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorFois : public OperatorBin {
public:
    OperatorFois(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorDiv : public OperatorBin {
public:
    OperatorDiv(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorMod : public OperatorBin {
public:
    OperatorMod(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

class OperatorPuiss : public OperatorBin {
public:
    OperatorPuiss(const Expression* left, const Expression* right);
    void visit(AstVisitor &visitor) const;
};

// ////////////////////////////////////////////////////////////////////////// //

#endif // _OPERATOR_H
