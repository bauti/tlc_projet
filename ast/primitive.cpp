#include "primitive.h"

// ////////////////////////////////////////////////////////////////////////// //

Primitive::Primitive(const ParamsList* params) :
        params(params) {

}

Primitive::~Primitive() {
    delete this->params;
}

const ParamsList* Primitive::getParams() const {
    return this->params;
}

// ////////////////////////////////////////////////////////////////////////// //

PrimAvance::PrimAvance(const ParamsList* params) :
        Primitive(params) {

}

void PrimAvance::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimRecule::PrimRecule(const ParamsList* params) :
        Primitive(params) {

}

void PrimRecule::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimTourneG::PrimTourneG(const ParamsList* params) :
        Primitive(params) {

}

void PrimTourneG::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimTourneD::PrimTourneD(const ParamsList* params) :
    Primitive(params) {

}

void PrimTourneD::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimLever::PrimLever(const ParamsList* params) :
        Primitive(params) {

}

void PrimLever::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimBaisser::PrimBaisser(const ParamsList* params) :
        Primitive(params) {

}

void PrimBaisser::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimSetCoul::PrimSetCoul(const ParamsList* params) :
        Primitive(params) {

}

void PrimSetCoul::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimSetPos::PrimSetPos(const ParamsList* params) :
        Primitive(params) {

}

void PrimSetPos::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //

PrimSetAngle::PrimSetAngle(const ParamsList* params) :
        Primitive(params) {

}

void PrimSetAngle::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

// ////////////////////////////////////////////////////////////////////////// //
