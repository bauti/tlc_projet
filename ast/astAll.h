#ifndef _AST_ALL_H
#define _AST_ALL_H

#include "bool.h"
#include "controle.h"
#include "expression.h"
#include "instruction.h"
#include "operator.h"
#include "primitive.h"

#endif // _AST_ALL_H
