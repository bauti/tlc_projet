#include "instruction.h"

// ////////////////////////////////////////////////////////////////////////// //

VariableCreate::VariableCreate(const char* type, const char* name) :
        Instruction(), type(type), name(name) {

}

void VariableCreate::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* VariableCreate::getType() const {
    return this->type;
}

const char* VariableCreate::getName() const {
    return this->name;
}

// ////////////////////////////////////////////////////////////////////////// //

Affect::Affect(const char* name, const Expression* value) :
        Instruction(), name(name), value(value) {

}

Affect::~Affect() {
    delete this->value;
}

void Affect::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* Affect::getName() const {
    return this->name;
}

const Expression* Affect::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

VariableCreateAffect::VariableCreateAffect(const char* type, const char* name, const Expression* value) :
        Instruction(), type(type), name(name), value(value) {

}

VariableCreateAffect::~VariableCreateAffect() {
    delete this->value;
}

void VariableCreateAffect::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* VariableCreateAffect::getType() const {
    return this->type;
}

const char* VariableCreateAffect::getName() const {
    return this->name;
}

const Expression* VariableCreateAffect::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

ObjectAttributeAffect::ObjectAttributeAffect(const char* object, const char* attribute, const Expression* value) :
        Instruction(), object(object), attribute(attribute), value(value) {

}

ObjectAttributeAffect::~ObjectAttributeAffect() {
    delete this->value;
}

void ObjectAttributeAffect::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* ObjectAttributeAffect::getObject() const {
    return this->object;
}

const char* ObjectAttributeAffect::getAttribute() const {
    return this->attribute;
}

const Expression* ObjectAttributeAffect::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

ObjectMethodCall::ObjectMethodCall(const char* object, const char* method, const ParamsList* params) :
        Expression(), object(object), method(method), parametres(params) {

}

ObjectMethodCall::~ObjectMethodCall() {
    delete this->parametres;
}

void ObjectMethodCall::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* ObjectMethodCall::getObject() const {
    return this->object;
}

const char* ObjectMethodCall::getMethod() const {
    return this->method;
}

const ParamsList* ObjectMethodCall::getParametres() const {
    return this->parametres;
}

// ////////////////////////////////////////////////////////////////////////// //

MethodCall::MethodCall(const char* method, const ParamsList* params) :
        Expression(), method(method), parametres(params) {

}

MethodCall::~MethodCall() {
    delete this->parametres;
}

void MethodCall::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* MethodCall::getMethod() const {
    return this->method;
}

const ParamsList* MethodCall::getParametres() const {
    return this->parametres;
}

// ////////////////////////////////////////////////////////////////////////// //
