#include "expression.h"

#include "tokenTypes.h"
#include "instruction.h"

// ////////////////////////////////////////////////////////////////////////// //

Entier::Entier(const int value) :
        Expression(), value(value) {

}

void Entier::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

int Entier::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

Flot::Flot(const float value) :
        Expression(), value(value) {

}

void Flot::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

float Flot::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

Booleen::Booleen(const bool value) :
        Expression(), value(value) {

}

void Booleen::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

bool Booleen::getValue() const {
    return this->value;
}

// ////////////////////////////////////////////////////////////////////////// //

Ident::Ident(const char* name) :
        Expression(), name(name) {

}

Ident::~Ident() {
    //delete name;
}

void Ident::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* Ident::getName() const {
    return this->name;
}

// ////////////////////////////////////////////////////////////////////////// //

ObjectAttributeCall::ObjectAttributeCall(const char* object, const char* attribute) :
        Expression(), object(object), attribute(attribute) {

}

void ObjectAttributeCall::visit(AstVisitor &visitor) const {
    visitor.visit(*this);
}

const char* ObjectAttributeCall::getObject() const {
    return this->object;
}

const char* ObjectAttributeCall::getAttribute() const {
    return this->attribute;
}

// ////////////////////////////////////////////////////////////////////////// //
