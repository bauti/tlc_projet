#ifndef _EXPRESSION_H
#define _EXPRESSION_H

#include "astBasic.h"

// ////////////////////////////////////////////////////////////////////////// //

class Entier : public Expression {
private:
    const int value;
public:
    Entier(const int value);
    void visit(AstVisitor &visitor) const;
    int getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class Flot : public Expression {
private:
    const float value;
public:
    Flot(const float value);
    void visit(AstVisitor &visitor) const;
    float getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class Booleen : public Expression {
private:
    const bool value;
public:
    Booleen(const bool value);
    void visit(AstVisitor &visitor) const;
    bool getValue() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class Ident : public Expression {
private:
    const char* name;
public:
    Ident(const char* name);
    ~Ident();
    void visit(AstVisitor &visitor) const;
    const char* getName() const;
};

// ////////////////////////////////////////////////////////////////////////// //

class ObjectAttributeCall : public Expression {
private:
    const char* object;
    const char* attribute;
public:
    ObjectAttributeCall(const char* object, const char* attribute);
    void visit(AstVisitor &visitor) const;
    const char* getObject() const;
    const char* getAttribute() const;
};


// ////////////////////////////////////////////////////////////////////////// //

#endif // _EXPRESSION_H
