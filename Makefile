FLEXSRC=parser/Logo.ll
FLEXCOUT=parser/lex.yy.c
FLEXHOUT=parser/Logo.ll.h
FLEXOBJ= $(FLEXCOUT:.c=.o)

BISONSRC=parser/Logo.yy
BISONPREFIX=parser/Logo
BISONCOUT=$(BISONPREFIX).tab.cc
BISONHOUT=$(BISONPREFIX).tab.hh
BISONOBJ= $(BISONCOUT:.cc=.o)

CC=g++-5
CFLAGS_BASIC=-I. -Wall -Wextra -ansi -pedantic -std=c++03
CFLAGS_FLEX_BASIC=-I. -ansi -pedantic -std=c++03
CFLAGS_LINK=-I.
LIBS=-lfl

LOGOLIBINCLUDE= include
LOGOLIBNAME= liblogo.a

EXE=Logo
HFILES= \
	ast/astAll.h \
	ast/astBasic.h \
	ast/bool.h \
	ast/controle.h \
	ast/expression.h \
	ast/instruction.h \
	ast/operator.h \
	ast/primitive.h \
	ast/tokenTypes.h \
	parser/LogoParser.h \
	symrec/all.h \
	symrec/ClassSymRec.h \
	symrec/data.h \
	symrec/MethodProperties.h \
	symrec/ObjectSymRec.h \
	symrec/SymRec.h \
	symrec/SymRecItem.h \
	symrec/SymRecScope.h \
	symrec/TypeAttribute.h \
	symrec/Type.h \
	turtle/TurtleDebug.h \
	turtle/Turtle.h \
	visitor/astVisitor.h \
	visitor/EvalAst.h \
	visitor/semanticCheck.h
CFILES= \
	visitor/semanticCheck.cpp \
	visitor/astVisitor.cpp \
	visitor/EvalAst.cpp \
	turtle/TurtleDebug.cpp \
	turtle/Turtle.cpp \
	symrec/ObjectSymRec.cpp \
	symrec/Type.cpp \
	symrec/SymRecItem.cpp \
	symrec/MethodProperties.cpp \
	symrec/TypeAttribute.cpp \
	symrec/SymRecScope.cpp \
	symrec/SymRec.cpp \
	symrec/ClassSymRec.cpp \
	parser/LogoParser.cpp \
	ast/tokenTypes.cpp \
	ast/expression.cpp \
	ast/controle.cpp \
	ast/bool.cpp \
	ast/primitive.cpp \
	ast/instruction.cpp \
	ast/operator.cpp \
	ast/astBasic.cpp
CMAIN=main.cpp
OBJFILES= $(CFILES:.cpp=.o)
OBJMAIN= $(CMAIN:.cpp=.o)

.PHONY: lib all debug clean depends flex bison bisonD

# ############################################################################ #

all: $(BISONCOUT) $(FLEXCOUT) $(EXE)
all: CFLAGS=$(CFLAGS_BASIC)
all: CFLAGS_FLEX=$(CFLAGS_FLEX_BASIC)

debug: bisonD $(FLEXCOUT) $(EXE)
debug: CFLAGS=$(CFLAGS_BASIC) -g
debug: CFLAGS_FLEX=$(CFLAGS_FLEX_BASIC) -g

lib: $(BISONCOUT) $(FLEXCOUT) $(EXE)
lib: CFLAGS=$(CFLAGS_BASIC)
lib: CFLAGS_FLEX=$(CFLAGS_FLEX_BASIC)

clean:
	@rm -rf $(FLEXCOUT) $(FLEXHOUT) $(FLEXOBJ) \
	        $(BISONCOUT) $(BISONHOUT) $(BISONOBJ) Logo.output  \
		    $(EXE) $(OBJFILES) $(OBJMAIN) \
		    $(LOGOLIBNAME) $(LOGOLIBINCLUDE)

depends:
	gcc -MM $(CFILES) $(CFLAGS_BASIC)

lib: $(FLEXOBJ) $(OBJFILES) $(HFILES)
	@echo Create $(LOGOLIBNAME) static lib...
	@ar rcs $(LOGOLIBNAME) $^
	@echo Add headers in $(LOGOLIBINCLUDE)/ folder...
	@mkdir -p $(LOGOLIBINCLUDE)
	@cp --parents -t $(LOGOLIBINCLUDE) $(HFILES)
	@echo Done.

# ############################################################################ #

$(FLEXCOUT): $(FLEXSRC) $(BISONCOUT)
	flex --header-file=$(FLEXHOUT) -o $(FLEXCOUT) $^

$(BISONCOUT): $(BISONSRC)
	bison -d $< -b $(BISONPREFIX)

bisonD: $(BISONSRC)
	bison -v -d $<

$(EXE): $(FLEXOBJ) $(OBJFILES) $(OBJMAIN)
	$(CC) -o $@ $^ $(CFLAGS_LINK) $(LIBS)

%.o:
	$(CC) -c -o $@ $< $(CFLAGS)

# ####################### Auto-generated, don't touch ######################## #
visitor/semanticCheck.o: visitor/semanticCheck.cpp visitor/semanticCheck.h \
 visitor/astVisitor.h ast/astAll.h ast/bool.h ast/expression.h \
 ast/astBasic.h ast/tokenTypes.h ast/controle.h ast/instruction.h \
 ast/operator.h ast/primitive.h symrec/all.h symrec/ObjectSymRec.h \
 symrec/ClassSymRec.h symrec/data.h ast/astBasic.h symrec/SymRec.h \
 ast/tokenTypes.h symrec/SymRecScope.h symrec/TypeAttribute.h \
 symrec/Type.h symrec/MethodProperties.h symrec/SymRecItem.h
visitor/astVisitor.o: visitor/astVisitor.cpp visitor/astVisitor.h ast/astAll.h \
 ast/bool.h ast/expression.h ast/astBasic.h visitor/astVisitor.h \
 ast/tokenTypes.h ast/controle.h ast/instruction.h ast/operator.h \
 ast/primitive.h
visitor/EvalAst.o: visitor/EvalAst.cpp visitor/EvalAst.h visitor/astVisitor.h \
 ast/astAll.h ast/bool.h ast/expression.h ast/astBasic.h ast/tokenTypes.h \
 ast/controle.h ast/instruction.h ast/operator.h ast/primitive.h \
 symrec/all.h symrec/ObjectSymRec.h symrec/ClassSymRec.h symrec/data.h \
 ast/astBasic.h symrec/SymRec.h ast/tokenTypes.h symrec/SymRecScope.h \
 symrec/TypeAttribute.h symrec/Type.h symrec/MethodProperties.h \
 symrec/SymRecItem.h turtle/Turtle.h

turtle/TurtleDebug.o: turtle/TurtleDebug.cpp turtle/TurtleDebug.h \
 turtle/Turtle.h
turtle/Turtle.o: turtle/Turtle.cpp turtle/Turtle.h

symrec/ObjectSymRec.o: symrec/ObjectSymRec.cpp symrec/ObjectSymRec.h \
 symrec/ClassSymRec.h symrec/data.h ast/astBasic.h visitor/astVisitor.h \
 symrec/SymRec.h ast/tokenTypes.h ast/astBasic.h ast/expression.h \
 symrec/SymRecScope.h symrec/TypeAttribute.h symrec/Type.h
symrec/Type.o: symrec/Type.cpp symrec/Type.h symrec/data.h ast/astBasic.h \
 visitor/astVisitor.h
symrec/SymRecItem.o: symrec/SymRecItem.cpp symrec/SymRecItem.h symrec/data.h \
 ast/astBasic.h visitor/astVisitor.h symrec/Type.h
symrec/MethodProperties.o: symrec/MethodProperties.cpp symrec/MethodProperties.h \
 ast/tokenTypes.h ast/astBasic.h visitor/astVisitor.h ast/expression.h
symrec/TypeAttribute.o: symrec/TypeAttribute.cpp symrec/TypeAttribute.h \
 symrec/data.h ast/astBasic.h visitor/astVisitor.h symrec/Type.h \
 symrec/ClassSymRec.h symrec/SymRec.h ast/tokenTypes.h ast/astBasic.h \
 ast/expression.h symrec/SymRecScope.h symrec/MethodProperties.h
symrec/SymRecScope.o: symrec/SymRecScope.cpp symrec/SymRecScope.h \
 symrec/TypeAttribute.h symrec/data.h ast/astBasic.h visitor/astVisitor.h \
 symrec/Type.h
symrec/SymRec.o: symrec/SymRec.cpp symrec/SymRec.h ast/astBasic.h \
 visitor/astVisitor.h ast/tokenTypes.h ast/astBasic.h ast/expression.h \
 symrec/data.h symrec/SymRecScope.h symrec/TypeAttribute.h symrec/Type.h
symrec/ClassSymRec.o: symrec/ClassSymRec.cpp symrec/ClassSymRec.h symrec/data.h \
 ast/astBasic.h visitor/astVisitor.h symrec/SymRec.h ast/tokenTypes.h \
 ast/astBasic.h ast/expression.h symrec/SymRecScope.h \
 symrec/TypeAttribute.h symrec/Type.h symrec/ObjectSymRec.h

parser/LogoParser.o: parser/LogoParser.cpp parser/LogoParser.h turtle/Turtle.h \
 symrec/SymRec.h ast/astBasic.h visitor/astVisitor.h ast/tokenTypes.h \
 ast/astBasic.h ast/expression.h symrec/data.h symrec/SymRecScope.h \
 symrec/TypeAttribute.h symrec/Type.h parser/Logo.ll.h parser/Logo.tab.hh \
 ast/astAll.h ast/bool.h ast/tokenTypes.h ast/controle.h \
 ast/instruction.h ast/operator.h ast/primitive.h symrec/all.h \
 symrec/ObjectSymRec.h symrec/ClassSymRec.h symrec/MethodProperties.h \
 symrec/SymRecItem.h visitor/semanticCheck.h visitor/EvalAst.h
parser/lex.yy.o: parser/lex.yy.c parser/Logo.tab.hh ast/tokenTypes.h \
 ast/astBasic.h visitor/astVisitor.h ast/expression.h ast/astAll.h \
 ast/bool.h ast/tokenTypes.h ast/controle.h ast/instruction.h \
 ast/operator.h ast/primitive.h symrec/all.h symrec/ObjectSymRec.h \
 symrec/ClassSymRec.h symrec/data.h ast/astBasic.h symrec/SymRec.h \
 symrec/SymRecScope.h symrec/TypeAttribute.h symrec/Type.h \
 symrec/MethodProperties.h symrec/SymRecItem.h turtle/TurtleDebug.h \
 turtle/Turtle.h visitor/semanticCheck.h visitor/EvalAst.h \
 turtle/Turtle.h parser/Logo.yy
parser/Logo.tab.o: Logo.tab.cc symrec/all.h symrec/ObjectSymRec.h \
 symrec/ClassSymRec.h symrec/data.h ast/astBasic.h visitor/astVisitor.h \
 symrec/SymRec.h ast/tokenTypes.h ast/astBasic.h ast/expression.h \
 symrec/SymRecScope.h symrec/TypeAttribute.h symrec/Type.h \
 symrec/MethodProperties.h symrec/SymRecItem.h ast/tokenTypes.h \
 ast/astAll.h ast/bool.h ast/expression.h ast/tokenTypes.h ast/controle.h \
 ast/instruction.h ast/astBasic.h ast/operator.h ast/primitive.h

ast/tokenTypes.o: ast/tokenTypes.cpp ast/tokenTypes.h ast/astBasic.h \
 visitor/astVisitor.h ast/expression.h
ast/expression.o: ast/expression.cpp ast/expression.h ast/astBasic.h \
 visitor/astVisitor.h ast/tokenTypes.h ast/instruction.h
ast/controle.o: ast/controle.cpp ast/controle.h ast/tokenTypes.h \
 ast/astBasic.h visitor/astVisitor.h ast/expression.h
ast/bool.o: ast/bool.cpp ast/bool.h ast/expression.h ast/astBasic.h \
 visitor/astVisitor.h ast/tokenTypes.h
ast/primitive.o: ast/primitive.cpp ast/primitive.h ast/tokenTypes.h \
 ast/astBasic.h visitor/astVisitor.h ast/expression.h
ast/instruction.o: ast/instruction.cpp ast/instruction.h ast/astBasic.h \
 visitor/astVisitor.h ast/tokenTypes.h ast/expression.h
ast/operator.o: ast/operator.cpp ast/operator.h ast/tokenTypes.h \
 ast/astBasic.h visitor/astVisitor.h ast/expression.h
ast/astBasic.o: ast/astBasic.cpp ast/astBasic.h visitor/astVisitor.h

main.o: main.cpp turtle/TurtleDebug.h turtle/Turtle.h parser/LogoParser.h \
 turtle/Turtle.h symrec/SymRec.h ast/astBasic.h visitor/astVisitor.h \
 ast/tokenTypes.h ast/astBasic.h ast/expression.h symrec/data.h \
 symrec/SymRecScope.h symrec/TypeAttribute.h symrec/Type.h
