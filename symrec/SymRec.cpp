#include "symrec/SymRec.h"

using namespace std;

SymRec::SymRec() :
        currentScope(new SymRecScope()) {

}

SymRec::~SymRec() {
    delete currentScope;
}

void SymRec::insert(std::string name, Type type, Data value) {
    this->currentScope->insert(name, type, value);
}

TypeAttribute* SymRec::lookup(std::string name) const {
    return this->currentScope->lookup(name);
}

void SymRec::enterScope() {
    this->currentScope = new SymRecScope(this->currentScope);
}

void SymRec::exitScope() {
    SymRecScope *oldScope = this->currentScope;
    this->currentScope = this->currentScope->getParent();
    oldScope->detachParent();
    delete oldScope;
}

map<string, TypeAttribute*> SymRec::getAllSymbols() const {
    return this->currentScope->getAllSymbols();
}
