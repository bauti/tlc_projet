#ifndef _METHOD_PROPERTIES_H
#define _METHOD_PROPERTIES_H

#include "ast/tokenTypes.h"

class MethodProperties {
protected:
    const char* returnType;
    const ParamsProtoList *params;
public:
    MethodProperties(const char* returnType, const ParamsProtoList *params);
    ~MethodProperties();
    const ParamsProtoList* getParams() const;
    const char* getReturnType() const;
};

#endif // _METHOD_PROPERTIES_H
