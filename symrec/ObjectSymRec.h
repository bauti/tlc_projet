#ifndef _OBJECT_SYMREC_H
#define _OBJECT_SYMREC_H

#include "symrec/ClassSymRec.h"

class ObjectSymRec : public ClassSymRec {
public:
    ObjectSymRec();
    ~ObjectSymRec();
};

#endif // _OBJECT_SYMREC_H
