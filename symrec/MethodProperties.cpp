#include "symrec/MethodProperties.h"

MethodProperties::MethodProperties(const char* returnType, const ParamsProtoList *params) :
        returnType(returnType), params(params) {

}

MethodProperties::~MethodProperties() {
    delete this->params;
}

const ParamsProtoList* MethodProperties::getParams() const {
    return this->params;
}

const char* MethodProperties::getReturnType() const {
    return this->returnType;
}
