#ifndef _TYPEATTRIBUTE_H
#define _TYPEATTRIBUTE_H

#include "symrec/data.h"
#include "symrec/Type.h"

class TypeAttribute {
private:
    bool preserveData;
public:
    const Type type;
    Data value;
    TypeAttribute(const Type type, Data value);
    TypeAttribute(const TypeAttribute& obj);
    void setPreserveData(bool preserveData);
    ~TypeAttribute();
};

#endif // _TYPEATTRIBUTE_H
