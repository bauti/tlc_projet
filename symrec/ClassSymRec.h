#ifndef _CLASS_SYMREC_H
#define _CLASS_SYMREC_H

#include "symrec/data.h"
#include "symrec/SymRec.h"
#include "symrec/Type.h"

#include <string>

class ObjectSymRec;

class ClassSymRec : public SymRec {
private:
    // Mask these functions to others
    inline void insert() {}
    inline void enterScope() {}
    inline void exitScope() {}
public:
    ClassSymRec();
    ~ClassSymRec();
    void addMethod(std::string name, Type type, Data value);
    void addAttribute(std::string name, Type type, Data value);
    ObjectSymRec* createVar();
};

#endif // _CLASS_SYMREC_H
