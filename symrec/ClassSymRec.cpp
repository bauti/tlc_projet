#include "symrec/ClassSymRec.h"

#include "symrec/ObjectSymRec.h"

using namespace std;

ClassSymRec::ClassSymRec() :
        SymRec() {
    SymRec::enterScope();

}

ClassSymRec::~ClassSymRec() {
}

void ClassSymRec::addMethod(std::string name, Type type, Data value) {
    this->currentScope->getParent()->insert(name, type, value);
}

void ClassSymRec::addAttribute(std::string name, Type type, Data value) {
    this->currentScope->insert(name, type, value);
}

ObjectSymRec* ClassSymRec::createVar() {
    ObjectSymRec *res = new ObjectSymRec();

    std::map<std::string, TypeAttribute*>::const_iterator it;

    // Shallow copy of methods (shared between all object of the same class)
    map<string, TypeAttribute*> methSymbols = this->currentScope->getParent()->getSymbols();
    for(it = methSymbols.begin(); it != methSymbols.end(); it++) {
        res->addMethod(it->first, it->second->type, it->second->value);
    }

    // Deep copy of atributes
    map<string, TypeAttribute*> attrSymbols = this->currentScope->getSymbols();
    for(it = attrSymbols.begin(); it != attrSymbols.end(); it++) {
        res->addAttribute(it->first, it->second->type, Data());
    }

    return res;
}
