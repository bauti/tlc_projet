#ifndef _SYMREC_H
#define _SYMREC_H

#include "ast/astBasic.h"
#include "ast/tokenTypes.h"

#include "symrec/data.h"
#include "symrec/SymRecScope.h"
#include "symrec/Type.h"

#include <string>
#include <deque>
#include <map>

class SymRec {
protected:
    SymRecScope* currentScope;
public:
    SymRec();
    ~SymRec();
    void insert(std::string name, Type type, Data value);
    TypeAttribute* lookup(std::string name) const;
    void enterScope();
    void exitScope();
    std::map<std::string, TypeAttribute*> getAllSymbols() const;
};

#endif // _SYMREC_H
