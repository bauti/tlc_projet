#include "symrec/TypeAttribute.h"

#include "ast/astBasic.h"
#include "symrec/ObjectSymRec.h"
#include "symrec/ClassSymRec.h"
#include "symrec/MethodProperties.h"

TypeAttribute::TypeAttribute(const Type type, const Data value) :
        preserveData(false), type(type), value(value) {

}

TypeAttribute::TypeAttribute(const TypeAttribute& obj) :
        preserveData(false), type(obj.type), value(obj.value) {

}

TypeAttribute::~TypeAttribute() {
    if(this->type.getType() == TYPE_FONCTION && !this->preserveData) {
        delete this->type.getInfos().typeMethod;
        delete this->value.ast;
    } else if(this->type.getType() == TYPE_CLASSE && !this->preserveData) {
        delete this->value.classSymRec;
    } else if(this->type.getType() == TYPE_OBJET && !this->preserveData) {
        delete this->value.objectSymRec;
    }
}

void TypeAttribute::setPreserveData(bool preserveData) {
    this->preserveData = preserveData;
}
