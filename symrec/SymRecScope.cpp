#include "symrec/SymRecScope.h"
#include <iostream>

using namespace std;

SymRecScope::SymRecScope(SymRecScope* parent) :
        preserveData(false), parent(parent) {

}

SymRecScope::~SymRecScope() {
    if(parent != NULL) {
        delete parent;
    }
    map<string,TypeAttribute*>::iterator it;
    for(it = this->symbols.begin(); it != this->symbols.end(); it++) {
        // Prevent delete this when exit object method
        if(it->first == "this" || this->preserveData) {
            it->second->setPreserveData(true);
        }
        delete it->second;
    }
}

void SymRecScope::insert(std::string name, Type type, Data value) {
    //cout << "name : " << name << endl;
    this->symbols[name] = new TypeAttribute(type, value);
}

TypeAttribute* SymRecScope::lookup(std::string name) const {
    map<string,TypeAttribute*>::const_iterator found = this->symbols.find(name);
    if(found != this->symbols.end()) {
        return found->second;
    } else if(this->parent == NULL) {
        return NULL;
    } else {
        return this->parent->lookup(name);
    }
}

void SymRecScope::setPreserveData(bool preserveData) {
    this->preserveData = preserveData;
}

void SymRecScope::detachParent() {
    this->parent = NULL;
}

SymRecScope* SymRecScope::getParent() const {
    return this->parent;
}

const map<std::string, TypeAttribute*> SymRecScope::getSymbols() const {
    return this->symbols;
}

map<string, TypeAttribute*> SymRecScope::getAllSymbols() const {
    map<string, TypeAttribute*> res;
    res.insert(this->symbols.begin(), this->symbols.end());
    if(this->parent != NULL) {
        map<string, TypeAttribute*> parentRes = this->parent->getAllSymbols();
        res.insert(parentRes.begin(), parentRes.end());
    }

    return res;
}
