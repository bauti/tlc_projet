#include "symrec/Type.h"

Type::Type(const TypeEnum type, const TypeData infos) :
        type(type), infos(infos) {

}

TypeEnum Type::getType() const {
    return this->type;
}

TypeData Type::getInfos() const {
    return this->infos;
}
