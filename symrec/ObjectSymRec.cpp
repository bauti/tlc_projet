#include "symrec/ObjectSymRec.h"

ObjectSymRec::ObjectSymRec() :
        ClassSymRec() {

}

ObjectSymRec::~ObjectSymRec() {
    // Prevent delete methods list (which should be deleted with ClassSymRec which created this ObejctSymRec)
    this->currentScope->getParent()->setPreserveData(true);
}
