#ifndef _SYMREC_DATA_H
#define _SYMREC_DATA_H

#include "ast/astBasic.h"

class SymRec;
class ClassSymRec;
class ObjectSymRec;
class MethodProperties;

union Data {
    int ent;
    float flot;
    bool booleen;
    AbstractAst *ast;
    SymRec *symrec;
    ClassSymRec* classSymRec;
    ObjectSymRec* objectSymRec;
};

enum TypeEnum {
    TYPE_BOOLEEN,
    TYPE_FONCTION,
    TYPE_OBJET,
    TYPE_ENTIER,
    TYPE_FLOT,
    TYPE_CLASSE
};

union TypeData {
    const char* typeObjet;
    MethodProperties *typeMethod;
};

#endif // _SYMREC_DATA_H
