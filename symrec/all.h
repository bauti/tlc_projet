#ifndef _SYMREC_ALL_H
#define _SYMREC_ALL_H

#include "symrec/ObjectSymRec.h"
#include "symrec/ClassSymRec.h"
// #include "symrec/data.h"
#include "symrec/MethodProperties.h"
#include "symrec/SymRec.h"
// #include "symrec/SymRecScope.h"
#include "symrec/SymRecItem.h"
// #include "symrec/Type.h"
// #include "symrec/TypeAttribute.h"

#endif // _SYMREC_ALL_H
