#ifndef _SYMREC_TYPE_H
#define _SYMREC_TYPE_H

#include "symrec/data.h"

class Type {
protected:
    const TypeEnum type;
    const TypeData infos;

public:
    Type(const TypeEnum type, const TypeData infos);
    TypeEnum getType() const;
    TypeData getInfos() const;
};

#endif // _SYMREC_TYPE_H
