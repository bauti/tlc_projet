#ifndef _SYMREC_ITEM_H
#define _SYMREC_ITEM_H

#include "symrec/data.h"
#include "symrec/Type.h"

#include <string>

class SymRecItem {
public:
    const std::string name;
    const Type type;
    const Data value;
    SymRecItem(const std::string name, const Type type, const Data value);
};

#endif // SYMREC_ITEM_H
