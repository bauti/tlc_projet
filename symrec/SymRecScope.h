#ifndef _SYMREC_SCOPE_H
#define _SYMREC_SCOPE_H

#include "symrec/TypeAttribute.h"

#include <map>
#include <string>

class SymRecScope {
private:
    bool preserveData;
protected:
    std::map<std::string, TypeAttribute*> symbols;
    SymRecScope* parent;
public:
    SymRecScope(SymRecScope* parent = NULL);
    ~SymRecScope();
    void insert(std::string name, Type type, Data value);
    TypeAttribute* lookup(std::string name) const;
    void setPreserveData(bool preserveData);
    void detachParent();
    SymRecScope* getParent() const;
    const std::map<std::string, TypeAttribute*> getSymbols() const;
    std::map<std::string, TypeAttribute*> getAllSymbols() const;
};

#endif // _SYMREC_SCOPE_H
