#ifndef _TURTLE_DEBUG_H
#define _TURTLE_DEBUG_H

#include "Turtle.h"

class TurtleDebug : public Turtle {
private:
    void showPos() const;
    void showAngle() const;
    void showCouleur() const;
    void showDrawing() const;
    void showAll() const;
public:
    TurtleDebug();
    ~TurtleDebug();
    void avance(float distance);
    void recule(float distance);
    void tourneGauche(float angle);
    void tourneDroite(float angle);
    void lever();
    void baisser();
    Couleur getCouleur() const;
    void setCouleur(float r, float g, float b);
    Coordonnees getPos() const;
    void setPos(float x, float y);
    float getAngle() const;
    void setAngle(float degre);
};

#endif // _TURTLE_DEBUG_H
