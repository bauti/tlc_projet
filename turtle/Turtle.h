#ifndef _TURTLE_H
#define _TURTLE_H

class Couleur {
public:
    float r;
    float g;
    float b;

    Couleur();
};

class Coordonnees {
public:
    float x;
    float y;

    Coordonnees();
};

class Turtle {
private:
    Coordonnees position;
    Couleur couleur;
    float angle;
    bool drawing;

    static float angleDeg2Rad(float angle);
protected:
    bool isDrawing() const;
public:
    Turtle();
    virtual ~Turtle();
    virtual void avance(float distance);
    virtual void recule(float distance);
    virtual void tourneGauche(float angle);
    virtual void tourneDroite(float angle);
    virtual void lever();
    virtual void baisser();
    virtual Couleur getCouleur() const;
    virtual void setCouleur(float r, float g, float b);
    virtual Coordonnees getPos() const;
    virtual void setPos(float x, float y);
    virtual float getAngle() const;
    virtual void setAngle(float degre);
};

#endif // _TURTLE_H
