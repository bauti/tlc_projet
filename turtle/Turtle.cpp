#include "Turtle.h"

#include <cmath>

using namespace std;

Couleur::Couleur() :
        r(0.0), g(0.0), b(0.0) {

}

Coordonnees::Coordonnees() :
        x(0.0), y(0.0) {

}

Turtle::Turtle() :
        position(Coordonnees()), couleur(Couleur()), angle(0), drawing(false) {

}

Turtle::~Turtle() {

}

float Turtle::angleDeg2Rad(float angle) {
    return angle * M_PI / 180.0;
}

bool Turtle::isDrawing() const {
    return this->drawing;
}

void Turtle::avance(float distance) {
    this->position.x += distance * cos(angleDeg2Rad(this->angle));
    this->position.y += distance * sin(angleDeg2Rad(this->angle));
}

void Turtle::recule(float distance) {
    this->position.x -= distance * cos(angleDeg2Rad(this->angle));
    this->position.y -= distance * sin(angleDeg2Rad(this->angle));
}

void Turtle::tourneGauche(float angle) {
    this->angle += angle;
}

void Turtle::tourneDroite(float angle) {
    this->angle -= angle;
}

void Turtle::lever() {
    this->drawing = false;
}

void Turtle::baisser() {
    this->drawing = true;
}

Couleur Turtle::getCouleur() const {
    return this->couleur;
}

void Turtle::setCouleur(float r, float g, float b) {
    this->couleur.r = r;
    this->couleur.g = g;
    this->couleur.b = b;
}

Coordonnees Turtle::getPos() const {
    return this->position;
}

void Turtle::setPos(float x, float y) {
    this->position.x = x;
    this->position.y = y;
}

float Turtle::getAngle() const {
    return this->angle;
}

void Turtle::setAngle(float degre) {
    this->angle = degre;
}
