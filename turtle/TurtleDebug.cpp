#include "TurtleDebug.h"

#include <iostream>

using namespace std;

TurtleDebug::TurtleDebug() :
        Turtle() {
    cout << "Création de la tortue" << endl;
    this->showAll();

}

TurtleDebug::~TurtleDebug() {

}

void TurtleDebug::showPos() const {
    Coordonnees coord = this->getPos();
    cout << "   Coordonnées de la tortue : (" << coord.x << "," << coord.y << ")" << endl;
}

void TurtleDebug::showAngle() const {
    float angle = this->getAngle();
    cout << "   Angle de la tortue : " << angle << " degrés" << endl;
}

void TurtleDebug::showCouleur() const {
    Couleur couleur = this->getCouleur();
    cout << "   Couleur de la tortue : (" << (int) (couleur.r * 255) << ","
                                          << (int) (couleur.g * 255) << ","
                                          << (int) (couleur.b * 255) << ")" << endl;
}

void TurtleDebug::showDrawing() const {
    bool isDrawing = this->isDrawing();
    if(isDrawing) {
        cout << "   La tortue écrit" << endl;
    } else {
        cout << "   La tortue n'écrit pas" << endl;
    }
}

void TurtleDebug::showAll() const {
    this->showPos();
    this->showAngle();
    this->showCouleur();
    this->showDrawing();
}


void TurtleDebug::avance(float distance) {
    Turtle::avance(distance);
    cout << "La tortue avance de " << distance << endl;
    this->showPos();
}

void TurtleDebug::recule(float distance) {
    Turtle::recule(distance);
    cout << "La tortue recule de " << distance << endl;
    this->showPos();
}

void TurtleDebug::tourneGauche(float angle) {
    Turtle::tourneGauche(angle);
    cout << "La tortue tourne à gauche de " << angle << " degrés" << endl;
    this->showAngle();
}

void TurtleDebug::tourneDroite(float angle) {
    Turtle::tourneDroite(angle);
    cout << "La tortue tourne à droite de " << angle << " degrés" << endl;
    this->showAngle();
}

void TurtleDebug::lever() {
    Turtle::lever();
    cout << "La tortue n'écrit plus maintenant" << endl;
}

void TurtleDebug::baisser() {
    Turtle::baisser();
    cout << "La tortue écrit maintenant" << endl;
}

Couleur TurtleDebug::getCouleur() const {
    return Turtle::getCouleur();
}

void TurtleDebug::setCouleur(float r, float g, float b) {
    Turtle::setCouleur(r,g,b);
    cout << "La tortue a une nouvelle couleur" << endl;
    this->showCouleur();
}

Coordonnees TurtleDebug::getPos() const {
    return Turtle::getPos();
}

void TurtleDebug::setPos(float x, float y) {
    Turtle::setPos(x,y);
    cout << "La tortue a une nouvelle position" << endl;
    this->showPos();
}

float TurtleDebug::getAngle() const {
    return Turtle::getAngle();
}

void TurtleDebug::setAngle(float degre) {
    Turtle::setAngle(degre);
    cout << "La tortue a un nouvel angle" << endl;
    this->showAngle();
}
