#ifndef _SEMANTIC_CHECK_H
#define _SEMANTIC_CHECK_H

#include "visitor/astVisitor.h"
#include "ast/astAll.h"
#include "symrec/all.h"

class SemanticCheck : public AstVisitor {
protected:

    char* typeCheck;
    SymRec* symrec;

    void visit(const SymRec* aSymrec);
    void visit(const ClassSymRec* aSymrec);
public:
    SemanticCheck(SymRec* symrec);
    ~SemanticCheck();
    void run();
    char* getTypeCheck() const;
    void setTypeCheck(char* value);


    // bool.h
    void visit(const BoolEt& ast);
    void visit(const BoolOu& ast);
    void visit(const BoolNon& ast);
    void visit(const CompEq& ast);
    void visit(const CompDf& ast);
    void visit(const CompPq& ast);
    void visit(const CompPe& ast);
    void visit(const CompGq& ast);
    void visit(const CompGe& ast);
    // controle.h
    void visit(const ControleIf& ast);
    void visit(const ControleFor& ast);
    void visit(const ControleWhile& ast);
    void visit(const ControleReturn& ast);
    // expression.h
    void visit(const Entier& ast);
    void visit(const Flot& ast);
    void visit(const Booleen& ast);
    void visit(const Ident& ast);
    void visit(const ObjectAttributeCall& ast);
    // instruction.h
    void visit(const VariableCreate& ast);
    void visit(const Affect& ast);
    void visit(const VariableCreateAffect& ast);
    void visit(const ObjectAttributeAffect& ast);
    void visit(const ObjectMethodCall& ast);
    void visit(const MethodCall& ast);
    // operator.h
    void visit(const OperatorPlus& ast);
    void visit(const OperatorMoins& ast);
    void visit(const OperatorFois& ast);
    void visit(const OperatorDiv& ast);
    void visit(const OperatorMod& ast);
    void visit(const OperatorPuiss& ast);
    // primitive.h
    void visit(const PrimAvance& ast);
    void visit(const PrimRecule& ast);
    void visit(const PrimTourneG& ast);
    void visit(const PrimTourneD& ast);
    void visit(const PrimLever& ast);
    void visit(const PrimBaisser& ast);
    void visit(const PrimSetCoul& ast);
    void visit(const PrimSetPos& ast);
    void visit(const PrimSetAngle& ast);
    // tokenTypes.h
    void visit(const InstructionList& ast);
    void visit(const ParamsList& ast);
    void visit(const ForParametres& ast);
    void visit(const ParamsProto& ast);
    void visit(const ParamsProtoList& ast);
};

#endif // _SEMANTIC_CHECK_H
