#ifndef _AST_VISITOR_H
#define _AST_VISITOR_H

class BoolEt;
class BoolOu;
class BoolNon;
class CompEq;
class CompDf;
class CompPq;
class CompPe;
class CompGq;
class CompGe;

class ControleIf;
class ControleFor;
class ControleWhile;
class ControleReturn;

class Entier;
class Flot;
class Booleen;
class Ident;
class ObjectAttributeCall;

class VariableCreate;
class Affect;
class VariableCreateAffect;
class ObjectAttributeAffect;
class ObjectMethodCall;
class MethodCall;

class OperatorPlus;
class OperatorMoins;
class OperatorFois;
class OperatorDiv;
class OperatorMod;
class OperatorPuiss;

class PrimAvance;
class PrimRecule;
class PrimTourneG;
class PrimTourneD;
class PrimLever;
class PrimBaisser;
class PrimSetCoul;
class PrimSetPos;
class PrimSetAngle;

class InstructionList;
class ParamsList;
class ParamsProto;
class ParamsProtoList;
class ForParametres;

class AstVisitor {
public:
    AstVisitor();
    ~AstVisitor();

    virtual void run() = 0;

    // bool.h
    virtual void visit(const BoolEt& ast) = 0;
    virtual void visit(const BoolOu& ast) = 0;
    virtual void visit(const BoolNon& ast) = 0;
    virtual void visit(const CompEq& ast) = 0;
    virtual void visit(const CompDf& ast) = 0;
    virtual void visit(const CompPq& ast) = 0;
    virtual void visit(const CompPe& ast) = 0;
    virtual void visit(const CompGq& ast) = 0;
    virtual void visit(const CompGe& ast) = 0;
    // controle.h
    virtual void visit(const ControleIf& ast) = 0;
    virtual void visit(const ControleFor& ast) = 0;
    virtual void visit(const ControleWhile& ast) = 0;
    virtual void visit(const ControleReturn& ast) = 0;
    // expression.h
    virtual void visit(const Entier& ast) = 0;
    virtual void visit(const Flot& ast) = 0;
    virtual void visit(const Booleen& ast) = 0;
    virtual void visit(const Ident& ast) = 0;
    virtual void visit(const ObjectAttributeCall& ast) = 0;
    // instruction.h
    virtual void visit(const VariableCreate& ast) = 0;
    virtual void visit(const Affect& ast) = 0;
    virtual void visit(const VariableCreateAffect& ast) = 0;
    virtual void visit(const ObjectAttributeAffect& ast) = 0;
    virtual void visit(const ObjectMethodCall& ast) = 0;
    virtual void visit(const MethodCall& ast) = 0;
    // operator.h
    virtual void visit(const OperatorPlus& ast) = 0;
    virtual void visit(const OperatorMoins& ast) = 0;
    virtual void visit(const OperatorFois& ast) = 0;
    virtual void visit(const OperatorDiv& ast) = 0;
    virtual void visit(const OperatorMod& ast) = 0;
    virtual void visit(const OperatorPuiss& ast) = 0;
    // primitive.h
    virtual void visit(const PrimAvance& ast) = 0;
    virtual void visit(const PrimRecule& ast) = 0;
    virtual void visit(const PrimTourneG& ast) = 0;
    virtual void visit(const PrimTourneD& ast) = 0;
    virtual void visit(const PrimLever& ast) = 0;
    virtual void visit(const PrimBaisser& ast) = 0;
    virtual void visit(const PrimSetCoul& ast) = 0;
    virtual void visit(const PrimSetPos& ast) = 0;
    virtual void visit(const PrimSetAngle& ast) = 0;
    // tokenTypes.h
    virtual void visit(const InstructionList& ast) = 0;
    virtual void visit(const ParamsList& ast) = 0;
    virtual void visit(const ParamsProto& ast) = 0;
    virtual void visit(const ParamsProtoList& ast) = 0;
    virtual void visit(const ForParametres& ast) = 0;

};

#endif // _AST_VISITOR_H
