#include "visitor/EvalAst.h"

#include <iostream>
#include <cstring>
#include <cmath>

#include <typeinfo>

using namespace std;

EvalAst::EvalAst(SymRec* symrec, Turtle *turtle) :
        AstVisitor(), symrec(symrec), turtle(turtle), lastRes(NULL),
        lastMethProto(NULL), returnVisited(false) {

}

EvalAst::~EvalAst() {

}

void EvalAst::run() {
    TypeAttribute *mainFunc = this->symrec->lookup("main");
    if(mainFunc != NULL && mainFunc->type.getType() == TYPE_FONCTION) {
        this->symrec->enterScope();
        mainFunc->value.ast->visit(*this);
        this->symrec->exitScope();
    } else {
        throw "Pas de fonction main() détectée, abandon.";
    }
}

// bool.h

void EvalAst::visit(const BoolEt& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;
    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    this->lastRes->value.booleen = leftValueTA->value.booleen && rightValueTA->value.booleen;

    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const BoolOu& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;
    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    this->lastRes->value.booleen = leftValueTA->value.booleen || rightValueTA->value.booleen;

    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const BoolNon& ast) {
    // Left
    ast.getExpr()->visit(*this);
    TypeAttribute *valueTA = this->lastRes;
    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    this->lastRes->value.booleen = valueTA->value.booleen;

    delete valueTA;
}

void EvalAst::visit(const CompEq& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent == rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot == rightValueTA->value.flot;
    } else if(leftValueTA->type.getType() == TYPE_BOOLEEN) {
        this->lastRes->value.booleen = leftValueTA->value.booleen == rightValueTA->value.booleen;
    } else {
        cerr << "<ERR> Comp == ERROR";
        exit(1);
    }

    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const CompDf& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent != rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot != rightValueTA->value.flot;
    } else if(leftValueTA->type.getType() == TYPE_BOOLEEN) {
        this->lastRes->value.booleen = leftValueTA->value.booleen != rightValueTA->value.booleen;
    } else {
        cerr << "<ERR> Comp != ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const CompPq& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent < rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot < rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Comp < ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const CompPe& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent <= rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot <= rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Comp <= ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const CompGq& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent > rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot > rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Comp > ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const CompGe& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    this->lastRes = new TypeAttribute(Type(TYPE_BOOLEEN, TypeData()), Data());
    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes->value.booleen = leftValueTA->value.ent >= rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes->value.booleen = leftValueTA->value.flot >= rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Comp >= ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

// controle.h

void EvalAst::visit(const ControleIf& ast) {
    ast.getCondition()->visit(*this);
    bool cond = this->lastRes->value.booleen;
    delete this->lastRes;
    if(cond) {
        ast.getTrueInstructions()->visit(*this);
    } else {
        if(ast.getFalseInstructions() != NULL) {
            ast.getFalseInstructions()->visit(*this);
        }
    }
}

void EvalAst::visit(const ControleFor& ast) {
    this->symrec->enterScope();
    ast.getParametres()->getInit()->visit(*this);

    bool cond;
    ast.getParametres()->getCondition()->visit(*this);
    cond = this->lastRes->value.booleen;
    delete this->lastRes;

    while(cond) {
        ast.getInstructions()->visit(*this);
        ast.getParametres()->getIncrement()->visit(*this);
        ast.getParametres()->getCondition()->visit(*this);
        cond = this->lastRes->value.booleen;
        delete this->lastRes;
    }
    this->symrec->exitScope();
}

void EvalAst::visit(const ControleWhile& ast) {
    bool cond;
    ast.getCondition()->visit(*this);
    cond = this->lastRes->value.booleen;
    delete this->lastRes;

    while(cond) {
        ast.getInstructions()->visit(*this);;
        ast.getCondition()->visit(*this);
        cond = this->lastRes->value.booleen;
        delete this->lastRes;
    }
}

void EvalAst::visit(const ControleReturn& ast) {
    if(ast.getResult() != NULL) {
        ast.getResult()->visit(*this);
    } else {
        this->lastRes = NULL;
    }
    this->returnVisited = true;
}

// expression.h

void EvalAst::visit(const Entier& ast) {
    Data value;
    TypeData infos;
    Type type(TYPE_ENTIER, infos);
    value.ent = ast.getValue();
    this->lastRes = new TypeAttribute(type, value);
}

void EvalAst::visit(const Flot& ast) {
    Data value;
    TypeData infos;
    Type type(TYPE_FLOT, infos);
    value.flot = ast.getValue();
    this->lastRes = new TypeAttribute(type, value);
}

void EvalAst::visit(const Booleen& ast) {
    Data value;
    TypeData infos;
    Type type(TYPE_BOOLEEN, infos);
    value.booleen = ast.getValue();
    this->lastRes = new TypeAttribute(type, value);
}

void EvalAst::visit(const Ident& ast) {
    TypeAttribute *ta = this->symrec->lookup(ast.getName());
    this->lastRes = new TypeAttribute(*ta);
}

void EvalAst::visit(const ObjectAttributeCall& ast) {
    TypeAttribute *obj = this->symrec->lookup(ast.getObject());
    ObjectSymRec *osr = obj->value.objectSymRec;
    TypeAttribute *attr = osr->lookup(ast.getAttribute());
    this->lastRes = new TypeAttribute(attr->type, attr->value);
}

// instruction.h

void EvalAst::visit(const VariableCreate& ast) {
    Data data;
    TypeData typedata;
    // Type primitif ?
    if(strcmp(ast.getType(), "bool") == 0) {
        Type objType(TYPE_BOOLEEN, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else if(strcmp(ast.getType(), "int") == 0) {
        Type objType(TYPE_ENTIER, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else if(strcmp(ast.getType(), "float") == 0) {
        Type objType(TYPE_FLOT, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else {
        // Type composite : chercher classe pour pouvoir copier la structure
        ClassSymRec *csr = this->symrec->lookup(ast.getType())->value.classSymRec;
        data.objectSymRec = csr->createVar();
        typedata.typeObjet = ast.getType();
        Type objType(TYPE_OBJET, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    }
}

void EvalAst::visit(const Affect& ast) {
    TypeAttribute *obj = this->symrec->lookup(ast.getName());

    ast.getValue()->visit(*this);
    obj->value = this->lastRes->value;
    delete this->lastRes;
}

void EvalAst::visit(const VariableCreateAffect& ast) {
    Data data;
    TypeData typedata;
    typedata.typeObjet = ast.getType();
    // Type primitif ?
    if(strcmp(ast.getType(), "bool") == 0) {
        Type objType(TYPE_BOOLEEN, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else if(strcmp(ast.getType(), "int") == 0) {
        Type objType(TYPE_ENTIER, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else if(strcmp(ast.getType(), "float") == 0) {
        Type objType(TYPE_FLOT, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    } else {
        // Type composite : chercher classe pour pouvoir copier la structure
        ClassSymRec *csr = this->symrec->lookup(ast.getType())->value.classSymRec;
        data.objectSymRec = csr->createVar();
        Type objType(TYPE_OBJET, typedata);
        this->symrec->insert(string(ast.getName()), objType, data);
    }

    TypeAttribute *obj = this->symrec->lookup(ast.getName());
    ast.getValue()->visit(*this);
    obj->value = this->lastRes->value;
    delete this->lastRes;
}

void EvalAst::visit(const ObjectAttributeAffect& ast) {
     TypeAttribute *obj = this->symrec->lookup(ast.getObject());
     ObjectSymRec *osr = obj->value.objectSymRec;
     TypeAttribute *attr = osr->lookup(ast.getAttribute());

     ast.getValue()->visit(*this);
     attr->value = this->lastRes->value;
     delete this->lastRes;
}

void EvalAst::visit(const ObjectMethodCall& ast) {
    // Get method
    TypeAttribute *obj = this->symrec->lookup(ast.getObject());
    ObjectSymRec *osr = obj->value.objectSymRec;
    TypeAttribute *meth = osr->lookup(ast.getMethod());
    AbstractAst *methCall = meth->value.ast;

    this->symrec->enterScope();
    this->symrec->insert("this", obj->type, obj->value);
    this->lastMethProto = meth->type.getInfos().typeMethod;
    ast.getParametres()->visit(*this);
    methCall->visit(*this);
    this->returnVisited = false;
    this->symrec->exitScope();
}

void EvalAst::visit(const MethodCall& ast) {
    TypeAttribute *meth = this->symrec->lookup(ast.getMethod());
    AbstractAst *methCall = meth->value.ast;

    this->symrec->enterScope();
    this->lastMethProto = meth->type.getInfos().typeMethod;
    ast.getParametres()->visit(*this);
    methCall->visit(*this);
    this->symrec->exitScope();
}

// operator.h

void EvalAst::visit(const OperatorPlus& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = leftValueTA->value.ent + rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes = new TypeAttribute(Type(TYPE_FLOT, TypeData()), Data());
        this->lastRes->value.flot = leftValueTA->value.flot + rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Op + ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const OperatorMoins& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = leftValueTA->value.ent - rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes = new TypeAttribute(Type(TYPE_FLOT, TypeData()), Data());
        this->lastRes->value.flot = leftValueTA->value.flot - rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Op - ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const OperatorFois& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = leftValueTA->value.ent * rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes = new TypeAttribute(Type(TYPE_FLOT, TypeData()), Data());
        this->lastRes->value.flot = leftValueTA->value.flot * rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Op * ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const OperatorDiv& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = leftValueTA->value.ent / rightValueTA->value.ent;
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes = new TypeAttribute(Type(TYPE_FLOT, TypeData()), Data());
        this->lastRes->value.flot = leftValueTA->value.flot / rightValueTA->value.flot;
    } else {
        cerr << "<ERR> Op / ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const OperatorMod& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = leftValueTA->value.ent % rightValueTA->value.ent;
    } else {
        cerr << "<ERR> Op % ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

void EvalAst::visit(const OperatorPuiss& ast) {
    // Left
    ast.getLeft()->visit(*this);
    TypeAttribute *leftValueTA = this->lastRes;
    // Right, then
    ast.getRight()->visit(*this);
    TypeAttribute *rightValueTA = this->lastRes;

    if(leftValueTA->type.getType() == TYPE_ENTIER) {
        this->lastRes = new TypeAttribute(Type(TYPE_ENTIER, TypeData()), Data());
        this->lastRes->value.ent = (int) pow(leftValueTA->value.ent, rightValueTA->value.ent);
    } else if(leftValueTA->type.getType() == TYPE_FLOT) {
        this->lastRes = new TypeAttribute(Type(TYPE_FLOT, TypeData()), Data());
        this->lastRes->value.flot = pow(leftValueTA->value.flot, rightValueTA->value.flot);
    } else {
        cerr << "<ERR> Op ^ ERROR";
        exit(1);
    }
    delete leftValueTA;
    delete rightValueTA;
}

// primitive.h

void EvalAst::visit(const PrimAvance& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // deg
    (*itE)->visit(*this);
    int pas = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->avance(pas);
}

void EvalAst::visit(const PrimRecule& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // deg
    (*itE)->visit(*this);
    int pas = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->recule(pas);
}

void EvalAst::visit(const PrimTourneG& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // deg
    (*itE)->visit(*this);
    int deg = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->tourneGauche(deg);
}

void EvalAst::visit(const PrimTourneD& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // deg
    (*itE)->visit(*this);
    int deg = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->tourneDroite(deg);
}

void EvalAst::visit(const PrimLever& ast) {
    this->turtle->lever();

    ast.getParams();
}

void EvalAst::visit(const PrimBaisser& ast) {
    this->turtle->baisser();

    ast.getParams();
}

void EvalAst::visit(const PrimSetCoul& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // r
    (*itE)->visit(*this);
    int r = this->lastRes->value.ent;
    delete this->lastRes;
    itE++;

    // g
    (*itE)->visit(*this);
    int g = this->lastRes->value.ent;
    delete this->lastRes;
    itE++;

    // b
    (*itE)->visit(*this);
    int b = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->setCouleur(r / 255.0, g / 255.0, b / 255.0);
}

void EvalAst::visit(const PrimSetPos& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    // x
    (*itE)->visit(*this);
    int x = this->lastRes->value.ent;
    delete this->lastRes;
    itE++;

    // y
    (*itE)->visit(*this);
    int y = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->setPos(x,y);
}

void EvalAst::visit(const PrimSetAngle& ast) {
    std::deque<const Expression*>::const_iterator itE;
    itE = ast.getParams()->getParametres();

    (*itE)->visit(*this);
    int deg = this->lastRes->value.ent;
    delete this->lastRes;

    this->turtle->setAngle(deg);
}

// tokenTypes.h

void EvalAst::visit(const InstructionList& ast) {
    std::deque<const Instruction*>::const_iterator it;

	for(it = ast.getInstructions(); it != ast.getInstructionsEnd(); it++) {
		(*it)->visit(*this);
        if(this->returnVisited) {
            this->returnVisited = false;
            return;
        }
	}
}

void EvalAst::visit(const ParamsList& ast) {
    std::deque<const Expression*>::const_iterator itE;
    std::deque<const ParamsProto*>::const_iterator itPP;

    const ParamsProtoList* protoParamList = this->lastMethProto->getParams();

	for(itE = ast.getParametres(), itPP = protoParamList->getParametres();
            itE != ast.getParametresEnd(); itE++, itPP++) {
		(*itE)->visit(*this);
        this->symrec->insert(string((*itPP)->getName()), this->lastRes->type, this->lastRes->value);
        delete this->lastRes;
	}
}

void EvalAst::visit(const ParamsProto& ast) {
    // Never used
    ast.getType();
    cerr << "<ERR> Params proto" << endl;
    exit(1);
}

void EvalAst::visit(const ParamsProtoList& ast) {
    // Never used
    ast.getParametres();
    cerr << "<ERR> Params proto list" << endl;
    exit(1);
}

void EvalAst::visit(const ForParametres& ast) {
    // Never used (for parameters in visit(ControleFor))
    ast.getCondition();
    cerr << "<ERR> Parametres FOR" << endl;
    exit(1);
}
