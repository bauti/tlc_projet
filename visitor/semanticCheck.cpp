#include "visitor/semanticCheck.h"

#include <iostream>
#include <string.h>

#define SIZEMAX_TYPE 41

using namespace std;


SemanticCheck::SemanticCheck(SymRec* symrec) :
        AstVisitor(), typeCheck((char*)""), symrec(symrec) {

}

SemanticCheck::~SemanticCheck() {

}

void SemanticCheck::run() {
    this->visit(this->symrec);
}

void SemanticCheck::visit(const SymRec* aSymrec) {

    std::map<std::string, TypeAttribute*> allSymbols = aSymrec->getAllSymbols();
    std::map<std::string, TypeAttribute*>::const_iterator it;

    for(it = allSymbols.begin(); it != allSymbols.end(); it++) {
        switch(it->second->type.getType()) {
            case TYPE_FONCTION: {
                // Ajout des symboles des parametres (dans une nouvelle portée)
                this->symrec->enterScope();
                const ParamsProtoList *PPL = it->second->type.getInfos().typeMethod->getParams();
                std::deque<const ParamsProto*>::const_iterator it2;
                for(it2 = PPL->getParametres(); it2 != PPL->getParametresEnd(); it2++) {
                    string typeParam = (*it2)->getType();
                    // Types primitifs
                    if(typeParam == "bool") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_BOOLEEN, TypeData()), Data());
                    } else if(typeParam == "int") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_ENTIER, TypeData()), Data());
                    } else if(typeParam == "float") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_FLOT, TypeData()), Data());
                    } else {
                        // Type composite : recherche classe puis création variable du type
                        TypeAttribute *TAParam = this->symrec->lookup(typeParam);
                        if(TAParam == NULL) {
                            cerr << "Unknown class " << typeParam;
                            throw "Unknown class";
                        } else if(TAParam->type.getType() != TYPE_CLASSE) {
                            cerr << "Symbol " << typeParam << " is not a class";
                            throw "Not a class";
                        }
                        TypeData typeData;
                        typeData.typeObjet = typeParam.c_str();
                        Data valueData;
                        valueData.objectSymRec = TAParam->value.classSymRec->createVar();
                        this->symrec->insert((*it2)->getName(), Type(TYPE_OBJET, typeData), valueData);
                    }
                }
                it->second->value.ast->visit(*this);
                this->symrec->exitScope();
                break;
            }
            case TYPE_CLASSE:
                this->visit(it->second->value.classSymRec);
                break;
            default:
                break;
        }
    }
}

void SemanticCheck::visit(const ClassSymRec *aSymrec) {
    std::map<std::string, TypeAttribute*> allSymbols = aSymrec->getAllSymbols();

    std::map<std::string, TypeAttribute*>::const_iterator it;
    for(it = allSymbols.begin(); it != allSymbols.end(); it++) {
        switch(it->second->type.getType()) {
            case TYPE_FONCTION: {
                this->symrec->enterScope();
                const ParamsProtoList *PPL = it->second->type.getInfos().typeMethod->getParams();
                std::deque<const ParamsProto*>::const_iterator it2;
                for(it2 = PPL->getParametres(); it2 != PPL->getParametresEnd(); it2++) {
                    string typeParam = (*it2)->getType();
                    // Types primitifs
                    if(typeParam == "bool") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_BOOLEEN, TypeData()), Data());
                    } else if(typeParam == "int") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_ENTIER, TypeData()), Data());
                    } else if(typeParam == "float") {
                        this->symrec->insert((*it2)->getName(), Type(TYPE_FLOT, TypeData()), Data());
                    } else {
                        // Type composite : recherche classe puis création variable du type
                        TypeAttribute *TAParam = this->symrec->lookup(typeParam);
                        if(TAParam == NULL) {
                            cerr << "Unknown class " << typeParam;
                            throw "Unknown class";
                        } else if(TAParam->type.getType() != TYPE_CLASSE) {
                            cerr << "Symbol " << typeParam << " is not a class";
                            throw "Not a class";
                        }
                        TypeData typeData;
                        typeData.typeObjet = typeParam.c_str();
                        Data valueData;
                        valueData.objectSymRec = TAParam->value.classSymRec->createVar();
                        this->symrec->insert((*it2)->getName(), Type(TYPE_OBJET, typeData), valueData);
                    }
                }
                this->visit(it->second->value.ast);
                this->symrec->exitScope();
                break;
            }
            default:
                break;
        }
    }
}

char* SemanticCheck::getTypeCheck() const {
	return this->typeCheck;
}

void SemanticCheck::setTypeCheck(char* value) {
	this->typeCheck = value;
}

// bool.h

void SemanticCheck::visit(const BoolEt& ast) {
	ast.getLeft()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "Opérateur ET, expression non booleenne à gauche.";
	}

	ast.getRight()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "Opérateur ET, expression non booleenne à droite.";
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const BoolOu& ast) {
	ast.getLeft()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "Opérateur OU, expression non bouleenne à gauche";
	}

	ast.getRight()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "Opérateur OU, expression non bouleenne à droite";
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const BoolNon& ast) {
	ast.getExpr()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "Opérateur NON, expression non booleenne";
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompEq& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if (( strcmp(this->typeCheck,"int"))
		&& ( strcmp(this->typeCheck,"float"))) {

		throw "Opérateur =, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur =, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur =, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompDf& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur !=, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur !=, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur !=, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompPq& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur <, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur <, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur <, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompPe& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur <=, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur <=, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur <=, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompGq& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur >, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur >, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur >, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const CompGe& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur >=, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur >=, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur >=, impossible de comparer deux expressions de type différentes";
			}
		}
	}

	this->setTypeCheck((char*)"bool");
}

// controle.h

void SemanticCheck::visit(const ControleIf& ast) {
	ast.getCondition()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "La condition dans un if doit être une expression booleenne";
	}

	ast.getTrueInstructions()->visit(*this);

	if (ast.getFalseInstructions() != NULL) {
		ast.getFalseInstructions()->visit(*this);
	}
}

void SemanticCheck::visit(const ControleFor& ast) {

	// Ce qui est créé dans une boucle for est spécifique à la boucle for

	this->symrec->enterScope();

	ast.getParametres()->visit(*this);
	ast.getInstructions()->visit(*this);

	this->symrec->exitScope();
}

void SemanticCheck::visit(const ControleWhile& ast) {
	ast.getCondition()->visit(*this);

	if (strcmp(this->typeCheck,"bool")) {
		throw "La condition dans un if doit être une expression booleenne";
	}

	ast.getInstructions()->visit(*this);
}

void SemanticCheck::visit(const ControleReturn& ast) {
	ast.getResult()->visit(*this);

	/*
		TO DO ?

	*/
}

// expression.h

void SemanticCheck::visit(const Entier& ast) {
    this->setTypeCheck((char*)"int");
}

void SemanticCheck::visit(const Flot& ast) {
    this->setTypeCheck((char*)"float");
}

void SemanticCheck::visit(const Booleen& ast) {
    this->setTypeCheck((char*)"bool");
}

void SemanticCheck::visit(const Ident& ast) {

	TypeAttribute* TA = this->symrec->lookup(ast.getName());

	if (TA == NULL) {
		cout << "Unable to find symbol " << ast.getName() << endl;
		throw "Unable to find symbol" ;
	}

	TypeEnum t = TA->type.getType();

	std::map<std::string, TypeAttribute*> allSymbols = this->symrec->getAllSymbols();
    std::map<std::string, TypeAttribute*>::const_iterator it;

	switch(t) {
		case TYPE_ENTIER:
			this->setTypeCheck((char*)"int");
			break;
		case TYPE_FLOT:
			this->setTypeCheck((char*)"float");
			break;
		case TYPE_BOOLEEN:
			this->setTypeCheck((char*)"bool");
			break;
		case TYPE_FONCTION:

			// on parcourt la table des symbboles pour trouver

			this->setTypeCheck((char*) it->second->type.getInfos().typeMethod->getReturnType());
			cout << "test retour fonction : " << this->getTypeCheck() << endl;

			break;
		case TYPE_OBJET:
			this->setTypeCheck((char*) it->second->type.getInfos().typeObjet);
			cout << "Test class of object : " << ast.getName() << " :" << this->getTypeCheck() << endl;			break;
			break;
		case TYPE_CLASSE:
			/*
				A gerer
			*/
			throw "Found a class";
			break;
		default:
			cout << "Default found on Ident check" << endl;
			break;
	}
}

void SemanticCheck::visit(const ObjectAttributeCall& ast) {

	/*
		A vérifier :
			- L'objet existe
			- L'attribut existe
	*/

	TypeAttribute* TA = this->symrec->lookup(ast.getObject());

	if (TA == NULL) {
		cout << "Unable to find symbol " << ast.getObject() << endl;
		throw "Unable to find symbol" ;
	}

	// L'object existe. On récupère sa classe.
	const char* objectClass = TA->type.getInfos().typeObjet;

	// On récupère les symboles de la classe
	std::map<std::string, TypeAttribute*> classSymbols = this->symrec->lookup(objectClass)->value.classSymRec->getAllSymbols();
	std::map<std::string, TypeAttribute*>::const_iterator it;

	it = classSymbols.find(ast.getAttribute());

	if (it == classSymbols.end()) {
		cout << "Attribut " << ast.getAttribute() << " not defined in class " << objectClass << endl;
		throw "Attribut not found";
	}

	TypeEnum TE = it->second->type.getType();

	switch (TE) {
		case TYPE_FONCTION:
			cout << "Method " << ast.getAttribute() << " missing parameters " << endl;
			throw "Method missing parameters";
			break;
		default:
			break;
	}

	// change typecheck ?

}

// instruction.h

void SemanticCheck::visit(const VariableCreate& ast) {

	// On vérifie si le type existe bien
	const char* typeVar = ast.getType();

	if ( ! ( (!strcmp(typeVar,"bool"))
		|| (!strcmp(typeVar,"int"))
		|| (!strcmp(typeVar,"float")) ) ) {

		// ce n'est pas un type primitif, donc on cherche si c'est  une classe
		TypeAttribute* TA = this->symrec->lookup(typeVar);

		if (TA == NULL) {
			cout << "Class " << typeVar << " not found" << endl;
			throw "Error, class not defined";
		}

		TypeEnum TE = TA->type.getType();

		if (TE != TYPE_CLASSE) {
			cout << "Class " << typeVar << " not found" << endl;
			throw "Error, class not defined";
		}
	}

	Data data;
    TypeData typedata;
    typedata.typeObjet = ast.getType();

    // Type primitif ?
    if(strcmp(ast.getType(), "bool") == 0) {
        Type objType(TYPE_BOOLEEN, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType(), "int") == 0) {
        Type objType(TYPE_ENTIER, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType(), "float") == 0) {
        Type objType(TYPE_FLOT, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else {
        // Type composite : chercher classe pour pouvoir copier la structure
        ClassSymRec *csr = this->symrec->lookup(ast.getType())->value.classSymRec;
        data.objectSymRec = csr->createVar();

        Type objType(TYPE_OBJET, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    }
}

void SemanticCheck::visit(const Affect& ast) {

	// on vérifie si le nom correspond bien à une variable qui existe
	TypeAttribute* TA = this->symrec->lookup(ast.getName());

	if (TA == NULL) {
		cout << "Unable to find symbol " << ast.getName() << endl;
		throw "Unable to find symbol" ;
	}

	// elle existe, on cherche son type
	TypeEnum TE = TA->type.getType();
	const char* varType;

	// on compare au type de l'expression
    ast.getValue()->visit(*this);

    switch(TE) {
    	case TYPE_ENTIER:
    		varType = "int";
    		break;
    	case TYPE_FLOT:
    		varType = "float";
    		break;
    	case TYPE_BOOLEEN:
    		varType = "bool";
    		break;
    	case TYPE_CLASSE:
    		cout << "Affectation to " << ast.getName() << " which is a class" << endl;
			throw "Variable is a class";
			break;
		case TYPE_FONCTION:
			cout << "Affectation to " << ast.getName() << " which is a function" << endl;
			throw "Variable is a function";
			break;
		case TYPE_OBJET:
			varType = TA->type.getInfos().typeObjet;
			break;
		default:
			break;
    }

    if (strcmp(this->typeCheck,varType) ) {
		cout << "Affectation to " << ast.getName() << endl;
		throw "Variable and expresion type do not correspond";
    }
}

void SemanticCheck::visit(const VariableCreateAffect& ast) {

	// On vérifie si le type existe bien
	const char* typeVar = ast.getType();

	if ( (strcmp(typeVar,"bool"))
		&& (strcmp(typeVar,"int"))
		&& (strcmp(typeVar,"float")) ) {

		// ce n'est pas un type primitif, donc on cherche si c'est une classe
		TypeAttribute* TA = this->symrec->lookup(typeVar);

		if (TA == NULL) {
			cout << "Class " << typeVar << " not found" << endl;
			throw "Error, class not defined";
		}

		TypeEnum TE = TA->type.getType();

		if (TE != TYPE_CLASSE) {
			cout << "Class " << typeVar << " not found" << endl;
			throw "Error, class not defined";
		}
	}

	// Le type de la déclaration est okay
	// On regarde celui de la valeur

	ast.getValue()->visit(*this);

	if (strcmp(this->typeCheck,typeVar)) {
		cout << "Affectation to " << ast.getName() << endl;
		throw "Variable and expresion type do not correspond";
    }

    Data data;
    TypeData typedata;
    typedata.typeObjet = ast.getType();

    // Type primitif ?
    if(strcmp(ast.getType(), "bool") == 0) {
        Type objType(TYPE_BOOLEEN, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType(), "int") == 0) {
        Type objType(TYPE_ENTIER, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType(), "float") == 0) {
        Type objType(TYPE_FLOT, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else {
        // Type composite : chercher classe pour pouvoir copier la structure
        ClassSymRec *csr = this->symrec->lookup(ast.getType())->value.classSymRec;
        data.objectSymRec = csr->createVar();

        Type objType(TYPE_OBJET, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    }
}

void SemanticCheck::visit(const ObjectAttributeAffect& ast) {

	/*
		A vérifier :
			- L'objet existe
			- L'attribut existe
			- le type de l'attribut et le type de la valeur corresponde
	*/

	TypeAttribute* TA = this->symrec->lookup(ast.getObject());

	if (TA == NULL) {
		cout << "Unable to find symbol " << ast.getObject() << endl;
		throw "Unable to find symbol" ;
	}

	// L'object existe. On récupère sa classe.
	const char* objectClass = TA->type.getInfos().typeObjet;

	// On récupère les symboles de la classe
	std::map<std::string, TypeAttribute*> classSymbols = this->symrec->lookup(objectClass)->value.classSymRec->getAllSymbols();
	std::map<std::string, TypeAttribute*>::const_iterator it;

	// On parcout les symboles pour trouver l'attribut
	it = classSymbols.find(ast.getAttribute());

	if (it == classSymbols.end()) {
		cout << "Attribut " << ast.getAttribute() << " not defined in class " << objectClass << endl;
		throw "Attribut not found";
	}

	TypeEnum TE = it->second->type.getType();

	// On regarde si la valeur correspond au type de l'attribut
	ast.getValue()->visit(*this);

	switch(TE) {
		case TYPE_ENTIER:
			if (strcmp(this->typeCheck,"int")) {
				cout << "Affected " << this->typeCheck << " to " << objectClass << " attribut of type int." << endl;
				throw "Error : wrong type in ObjectAttributeAffect";
			}
			break;
		case TYPE_FLOT:
			if (strcmp(this->typeCheck,"float")) {
				cout << "Affected " << this->typeCheck << " to " << objectClass << " attribut of type float" << endl;
				throw "Error : wrong type in ObjectAttributeAffect";
			}
			break;
		case TYPE_BOOLEEN:
			if (strcmp(this->typeCheck,"bool")) {
				cout << "Affected " << this->typeCheck << " to " << objectClass << " attribut of type bool" << endl;
				throw "Error : wrong type in ObjectAttributeAffect";
			}
			break;
		default:
			cout << "Affected " << this->typeCheck << " to " << objectClass << " attribut of non primitive type" << endl;
			throw "Error : wrong type in ObjectAttributeAffect";
    }
}

void SemanticCheck::visit(const ObjectMethodCall& ast) {

	/*
	 	On vérifie dans la visite de la ParamList si les arguments existent bien
	 	On vérifie dans le reste de ObjectMethodCall si leur type correspondent à ce qui est attendu
		On part donc du principe que les paramètres ont bien été défini
	*/

	ast.getParametres()->visit(*this);

	/*
		A vérifier :
			- L'objet existe
			- La méthode existe
			- Il y a le bon nombre de param
			- Le type des params correspond
	*/

	TypeAttribute* TA = this->symrec->lookup(ast.getObject());

	if (TA == NULL) {
		cout << "Unable to find symbol " << ast.getObject() << endl;
		throw "Unable to find symbol" ;
	}

	// L'object existe. On récupère sa classe.
	const char* objectClass = this->symrec->lookup(ast.getObject())->type.getInfos().typeObjet;

	// On récupère les symboles de la classe
	std::map<std::string, TypeAttribute*> classSymbols = this->symrec->lookup(objectClass)->value.classSymRec->getAllSymbols();
	std::map<std::string, TypeAttribute*>::const_iterator it;

	// On parcout les symboles pour trouver l'attribut
	it = classSymbols.find(ast.getMethod());

	if (it == classSymbols.end()) {
		cout << "Method " << ast.getMethod() << " not defined in class " << objectClass << endl;
		throw "Method not found";
	}

	// on récupère les paramètres de la méthode
	TypeEnum TE = it->second->type.getType();

	if (TE != TYPE_FONCTION) {
		cout << ast.getMethod() << " is not a method in class " << objectClass << endl;
		throw "Wrong call on object";
	}

	const ParamsProtoList* PPL = it->second->type.getInfos().typeMethod->getParams();
	const ParamsList* PL = ast.getParametres();

	if (PPL->getSize() != PL->getSize()) {
		cout << "Prototype for " << ast.getMethod() << " require " << PPL->getSize() << " arguments but got " << PL->getSize() << endl;
		throw "Wrong nomber of arguments";
	}

	std::deque<const ParamsProto*>::const_iterator itProto;
	std::deque<const Expression*>::const_iterator itCall;

		// On vérifie un à un si les types des arguments correspondent à ce qui est attendu

	for(itProto = PPL->getParametres(), itCall = PL->getParametres();
            itProto != PPL->getParametresEnd(); itProto++, itCall++) {

        const char* protoType = (*itProto)->getType().c_str(); // type requis dans le prototype de la méthode
    	(*itCall)->visit(*this);

    	if (strcmp(protoType,this->typeCheck)) {
    		cout << "In method " << ast.getMethod() << endl;
    		cout << "Argument " << (*itProto)->getName() << " of type " << protoType << " was passed argument of type " << this->typeCheck << endl;
    		throw "Wrong type for argument";
    	}
    }
}

void SemanticCheck::visit(const MethodCall& ast) {

	/*
	 	On vérifie dans le reste de MethodCall si leur type correspondent à ce qui est attendu
		On part donc du principe que les paramètres ont bien été défini
	*/

	ast.getParametres()->visit(*this);

	/*
		A vérifier :
			- La fonction existe
			- Il y a le bon nombre de param
			- Le type des params correspond
	*/

	// On cherche si la fonction existe bien

	TypeAttribute* TA = this->symrec->lookup(ast.getMethod());

	if (TA == NULL) {
		cout << "Function " << ast.getMethod() << " not defined." << endl;
		throw "Function not defined";
	}

	// on récupère les paramètres de la function
	TypeEnum TE = TA->type.getType();

	if (TE != TYPE_FONCTION) {
		cout << ast.getMethod() << " is not a function." << endl;
		throw "Wrong call";
	}

	const ParamsProtoList* PPL = TA->type.getInfos().typeMethod->getParams();
	const ParamsList* PL = ast.getParametres();

	if (PPL->getSize() != PL->getSize()) {
		cout << "Prototype for " << ast.getMethod() << " require " << PPL->getSize() << " arguments but got " << PL->getSize() << endl;
		throw "Wrong nomber of arguments";
	}

	std::deque<const ParamsProto*>::const_iterator itProto;
	std::deque<const Expression*>::const_iterator itCall;

		// On vérifie un à un si les types des arguments correspondent à ce qui est attendu

	for(itProto = PPL->getParametres(), itCall = PL->getParametres();
            itProto != PPL->getParametresEnd(); itProto++, itCall++) {

        const char* protoType = (*itProto)->getType().c_str(); // type requis dans le prototype de la méthode
    	(*itCall)->visit(*this);

    	if (strcmp(protoType,this->typeCheck)) {
    		cout << "In method " << ast.getMethod() << endl;
    		cout << "Argument " << (*itProto)->getName() << " of type " << protoType << " was passed argument of type " << this->typeCheck << endl;
    		throw "Wrong type for argument";
    	}
    }
}

// operator.h

void SemanticCheck::visit(const OperatorPlus& ast) {

    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur +, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur +, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur +, impossible avec deux expressions de types différents";
			}
		}
	}
}

void SemanticCheck::visit(const OperatorMoins& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur -, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur -, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur -, impossible avec deux expressions de types différents";
			}
		}
	}
}

void SemanticCheck::visit(const OperatorFois& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur *, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur *, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur *, impossible avec deux expressions de types différents";
			}
		}
	}
}

void SemanticCheck::visit(const OperatorDiv& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur /, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur /, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur /, impossible avec deux expressions de types différents";
			}
		}
	}
}

void SemanticCheck::visit(const OperatorMod& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur %, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur %, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur %, impossible avec deux expressions de types différents";
			}
		}
	}
}

void SemanticCheck::visit(const OperatorPuiss& ast) {
    char temp[SIZEMAX_TYPE];

	ast.getLeft()->visit(*this);

	if ((strcmp(this->typeCheck,"int"))
		&& (strcmp(this->typeCheck,"float"))) {

		throw "Opérateur ^, expression à gauche n'est pas numérique";
	}
	else {
		strcpy(temp,this->typeCheck);

		ast.getRight()->visit(*this);

		if ((strcmp(this->typeCheck,"int"))
			&& (strcmp(this->typeCheck,"float"))) {

			throw "Opérateur ^, expression à droite n'est pas numérique";
		}
		else {
			if (strcmp(temp,this->typeCheck)) {
				throw "Opérateur ^, impossible avec deux expressions de types différents";
			}
		}
	}
}

// primitive.h

void SemanticCheck::visit(const PrimAvance& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 1) {
		cout << "Wrong number of arguments for function PrimAvance. Expecting 1, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	if (strcmp(this->getTypeCheck(),"int")) {
		throw "Avancer primitive takes on argument of type int";
	}
}

void SemanticCheck::visit(const PrimRecule& ast) {

	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 1) {
		cout << "Wrong number of arguments for function PrimReculer. Expecting 1, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	if (strcmp(this->getTypeCheck(),"int")) {
		throw "Reculer primitive takes on argument of type int";
	}

}

void SemanticCheck::visit(const PrimTourneG& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 1) {
		cout << "Wrong number of arguments for function gauche. Expecting 1, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	if (strcmp(this->getTypeCheck(),"int")) {
		cout << "Found argument of type " << this->getTypeCheck() << endl;
		throw "gauche primitive takes on argument of type int";
	}

}

void SemanticCheck::visit(const PrimTourneD& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 1) {
		cout << "Wrong number of arguments for function droite. Expecting 1, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	if (strcmp(this->getTypeCheck(),"int")) {
		cout << "Found argument of type " << this->getTypeCheck() << endl;
		throw "droite primitive takes on argument of type int";
	}
}

void SemanticCheck::visit(const PrimLever& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 0) {
		cout << "Wrong number of arguments for function lever. Expecting 0, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}
}

void SemanticCheck::visit(const PrimBaisser& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 0) {
		cout << "Wrong number of arguments for function baisser. Expecting 0, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}
}

void SemanticCheck::visit(const PrimSetCoul& ast) {
	const ParamsList* PL = ast.getParams();
	PL->visit(*this);

	if (PL->getSize() != 3) {
		cout << "Wrong number of arguments for function setCouleur. Expecting 3, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	std::deque<const Expression*>::const_iterator it;

	for(it = PL->getParametres(); it != PL->getParametresEnd(); it++) {
		(*it)->visit(*this);

		if (strcmp(this->getTypeCheck(),"int")) {
			cout << "Found argument of type " << this->getTypeCheck() << endl;
			throw "setCouleur primitive takes arguments of type (int,int,int)";
		}
	}
}

void SemanticCheck::visit(const PrimSetPos& ast) {

	const ParamsList* PL = ast.getParams();
	PL->visit(*this);

	if (PL->getSize() != 2) {
		cout << "Wrong number of arguments for function setPos. Expecting 2, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	std::deque<const Expression*>::const_iterator it;

	for(it = PL->getParametres(); it != PL->getParametresEnd(); it++) {
		(*it)->visit(*this);

		if (strcmp(this->getTypeCheck(),"int")) {
			cout << "Found argument of type " << this->getTypeCheck() << endl;
			throw "setPos primitive takes arguments of type (int,int)";
		}
	}
}

void SemanticCheck::visit(const PrimSetAngle& ast) {
	ast.getParams()->visit(*this);

	if (ast.getParams()->getSize() != 1) {
		cout << "Wrong number of arguments for function setAngle. Expecting 1, got " << ast.getParams()->getSize() << endl;
		throw "Wrong number of arguments";
	}

	if (strcmp(this->getTypeCheck(),"int")) {
		cout << "Found argument of type " << this->getTypeCheck() << endl;
		throw "SetAngle primitive takes one argument of type int";
	}
}

// tokenTypes.h

void SemanticCheck::visit(const InstructionList& ast) {
	std::deque<const Instruction*>::const_iterator it;

	for(it = ast.getInstructions(); it != ast.getInstructionsEnd(); it++) {
		(*it)->visit(*this);
	}
}

void SemanticCheck::visit(const ParamsList& ast) {
	std::deque<const Expression*>::const_iterator it;

	for(it = ast.getParametres(); it != ast.getParametresEnd(); it++) {
		(*it)->visit(*this);
	}
}

void SemanticCheck::visit(const ParamsProto& ast)  {

	/*
		On check si le type exste bien
		Et on l'ajoute dans la table des symboles
	*/

	TypeAttribute* TA = this->symrec->lookup(ast.getName());

	if (TA == NULL) {
		cout << "Unable to find type " << ast.getType() << endl;
		throw "Unable to find type in parameter list" ;
	}

	TypeEnum t = TA->type.getType();

	std::map<std::string, TypeAttribute*> allSymbols = this->symrec->getAllSymbols();
    std::map<std::string, TypeAttribute*>::const_iterator it;

	switch(t) {
		case TYPE_FONCTION:
			cout << "Passing " << ast.getType() << " as argument type, but it's a function" << endl;
			throw "Passing function as a type for argument type" ;
			break;
		case TYPE_OBJET:
			cout << "Passing " << ast.getType() << " as argument type, but it's an object" << endl;
			throw "Passing object as a type for argument type" ;
			break;
		default:
			cout << "Default found on Ident check" << endl;
			break;
	}

	Data data;
    TypeData typedata;
    typedata.typeObjet = ast.getType().c_str();

    // Type primitif ?
    if(strcmp(ast.getType().c_str(), "bool") == 0) {
        Type objType(TYPE_BOOLEEN, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType().c_str(), "int") == 0) {
        Type objType(TYPE_ENTIER, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else if(strcmp(ast.getType().c_str(), "float") == 0) {
        Type objType(TYPE_FLOT, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    } else {
        // Type composite : chercher classe pour pouvoir copier la structure
        ClassSymRec *csr = this->symrec->lookup(ast.getType())->value.classSymRec;
        data.objectSymRec = csr->createVar();

        Type objType(TYPE_OBJET, typedata);
        this->symrec->insert(ast.getName(), objType, data);
    }

}

void SemanticCheck::visit(const ParamsProtoList& ast)  {
    std::deque<const ParamsProto*>::const_iterator it;

    this->symrec->enterScope();

	for(it = ast.getParametres(); it != ast.getParametresEnd(); it++) {
		(*it)->visit(*this);
	}

	this->symrec->exitScope();
}

void SemanticCheck::visit(const ForParametres& ast)  {


	ast.getInit()->visit(*this);
	ast.getCondition()->visit(*this);
	ast.getIncrement()->visit(*this);
}
