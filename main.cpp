#include "turtle/TurtleDebug.h"
#include "parser/LogoParser.h"

#include <queue>
#include <iostream>

int main(int argc, char** argv) {
    LogoParser logo;
    for(int i = 1; i < argc; i++) {
        logo.addFile(argv[i]);
    }
    logo.check();
    TurtleDebug *turtle = new TurtleDebug();
    logo.run(turtle);
}
