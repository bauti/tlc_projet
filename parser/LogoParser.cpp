#include "parser/LogoParser.h"

#include "Logo.ll.h"
#include "Logo.tab.hh"
#include "visitor/semanticCheck.h"
#include "visitor/EvalAst.h"

#include <iostream>

using namespace std;

LogoParser::LogoParser() :
        fileQueue(), globalSymRec(new SymRec()) {

}

LogoParser::~LogoParser() {
    delete globalSymRec;
}

void LogoParser::addFile(const char* filePath) {
    fileQueue.push(filePath);
}

void LogoParser::check() {
    // Lexical and syntax check
    while(!fileQueue.empty()) {
        const char* filename = fileQueue.front();
        yyin = fopen(filename,"r");
        if(yyin == NULL) {
            cerr << "<ERROR> Unknown file " << filename << endl;
            exit(1);
        }
        fileQueue.pop();
        cout << "Analyse de " << filename << "..." << endl;
        try {
            yyparse(globalSymRec, &fileQueue);
        } catch(const char* msg) {
            cerr << "<ERROR> " << msg << endl;
            exit(1);
        }

        fclose(yyin);
    }

    // Semantic check
    SemanticCheck semCheck(globalSymRec);
    try {
        semCheck.run();
    } catch(const char* msg) {
        cerr << "<ERROR> " << msg << endl;
        exit(1);
    }
    cout << "<OK> Les fichiers sont correctes" << endl;
    cout << "==============================================" << endl;
}

void LogoParser::run(Turtle *turtle) {
    // Run the turtle !
    EvalAst eval(globalSymRec, turtle);
    try {
        eval.run();
    } catch(const char* msg) {
        cerr << "<ERROR> " << msg << endl;
        exit(1);
    }
}
