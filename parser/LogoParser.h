#ifndef _LOGO_PARSER_H
#define _LOGO_PARSER_H

#include "turtle/Turtle.h"
#include "symrec/SymRec.h"

#include <queue>

class LogoParser {
protected:
    std::queue<const char*> fileQueue;
    SymRec *globalSymRec;
public:
    LogoParser();
    ~LogoParser();
    void addFile(const char* filePath);
    void check();
    void run(Turtle *turtle);
};

#endif // _LOGO_PARSER_H
