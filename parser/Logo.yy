%{
#include <iostream>
#include <cstdlib>

#include <queue>
#include "symrec/all.h"

extern int yylineno;

int yyerror(const SymRec* symrec, std::queue<const char*> *fileQueue, const char* msg) {
    std::cout << "<ERROR> line " << yylineno << " : " << msg << std::endl;
    delete symrec;
    delete fileQueue;
    exit(1);
}

%}

%parse-param {SymRec *globalSymRec} {std::queue<const char*> *fileQueue}

%union {
    const char* str;
    float flot;
    int ent;
    bool booleen;
    AbstractAst* ast;
    Instruction* instr;
    InstructionList* instrList;
    ParamsProtoList* paramsProto;
    ParamsProto* paramProto;
    ParamsList* params;
    Expression* expr;
    ForParametres* forParams;
    ClassSymRec* classSymRec;
    SymRec* symRec;
    SymRecItem* srItem;
}

%define parse.error verbose

%code requires {
    #include "ast/tokenTypes.h"
    #include "ast/astAll.h"
    #include "symrec/all.h"

    #include <queue>
    #include <cstring>

    extern int yylex(void);
}

%token MC_FOR
%token MC_WHILE
%token MC_IF
%token MC_ELSE
%token MC_CLASS
%token MC_RETURN
%token MC_INCLUDE
%token MC_THIS
%token MC_FUNCTION
%token<booleen> MC_TRUE
%token<booleen> MC_FALSE

%token ACCOL_OUV
%token ACCOL_FER
%token PAR_OUV
%token PAR_FER
%token CROCH_OUV
%token CROCH_FER
%token PV
%token PT
%token VIRG
%token AFFECT

%token OP_PLUS
%token OP_MOINS
%token OP_FOIS
%token OP_DIV
%token OP_MOD
%token OP_PUISS

%token COMP_EQ
%token COMP_DF
%token COMP_PQ
%token COMP_PE
%token COMP_GQ
%token COMP_GE

%token BOOL_ET
%token BOOL_OU
%token BOOL_NON

%token TYPE_INT
%token TYPE_FLOAT
%token TYPE_BOOL
%token TYPE_COUL
%token TYPE_VOID

%token PRIM_AV
%token PRIM_RE
%token PRIM_TG
%token PRIM_TD
%token PRIM_LC
%token PRIM_BC
%token PRIM_COUL
%token PRIM_SETCOUL
%token PRIM_POS
%token PRIM_ANGL
%token PRIM_SETPOS
%token PRIM_SETANGL

%token<str> IDENT
%token<ent> ENTIER
%token<flot> FLOT
%token<str> NOMFICHIER


%type<classSymRec> blocClasse
%type<srItem> classe
%type<classSymRec> listeInstructionClasse
%type<srItem> instructionClasse
%type<srItem> attribut
%type<srItem> methode
%type<paramsProto> listeParametreProto
%type<paramsProto> listeParametreProto2
%type<paramProto> parametreProto
%type<instrList> listeInstruction
%type<instrList> bloc
%type<str> typeAll
%type<str> type
%type<forParams> forParametres
%type<instrList> listeForInstruction
%type<instr> allInstruction
%type<instr> blocControle
%type<instr> instruction
%type<instr> primitive
%type<params> listeParametreAppel
%type<params> listeParametreAppel2
%type<expr> parametreAppel
%type<str> objet
%type<expr> expression

%left OP_PLUS OP_MOINS
%left OP_FOIS OP_DIV OP_MOD
%right OP_PUISS

%nonassoc COMP_EQ
%nonassoc COMP_DF
%nonassoc COMP_PQ
%nonassoc COMP_PE
%nonassoc COMP_GQ
%nonassoc COMP_GE

%left BOOL_ET
%left BOOL_OU
%right BOOL_NON

%%

fichier :
      listeInclude listeClasse listeMethode {}
    ;

listeInclude :
      include PV listeInclude {}
    | {}
    ;

include :
      MC_INCLUDE NOMFICHIER {fileQueue->push($2);}
    ;

listeClasse :
      classe listeClasse {globalSymRec->insert($1->name, $1->type, $1->value); delete $1;}
    | {}
    ;

classe :
      MC_CLASS IDENT blocClasse {
        Type type(TYPE_CLASSE, TypeData());
        Data data;
        data.classSymRec = $3;
        $$ = new SymRecItem($2, type, data);
      }
    ;

blocClasse :
      ACCOL_OUV listeInstructionClasse ACCOL_FER {$$ = $2;}
    ;

listeInstructionClasse :
      instructionClasse listeInstructionClasse {
          $$ = $2;
          if($1->type.getType() == TYPE_FONCTION) {
              $$->addMethod($1->name, $1->type, $1->value);
          } else {
              $$->addAttribute($1->name, $1->type, $1->value);
          }
          delete $1;
      }
    | {$$ = new ClassSymRec(); }
    ;

instructionClasse :
      attribut {$$ = $1;}
    | methode {$$ = $1;}
    ;

attribut :
      type IDENT PV {
        TypeData typeValue;
        Data data;
        typeValue.typeObjet = $1;
        if(strcmp($1, "bool") == 0) {
            Type type(TYPE_BOOLEEN, typeValue);
            $$ = new SymRecItem($2, type, data);
        } else if(strcmp($1, "int") == 0) {
            Type type(TYPE_ENTIER, typeValue);
            $$ = new SymRecItem($2, type, data);
        } else if(strcmp($1, "float") == 0) {
            Type type(TYPE_FLOT, typeValue);
            $$ = new SymRecItem($2, type, data);
        } else {
            throw "Type non primitif pour les attributs";
        }
    }
    ;

listeMethode :
      methode listeMethode {globalSymRec->insert($1->name, $1->type, $1->value); delete $1;}
    | {}
    ;

methode :
      typeAll MC_FUNCTION IDENT PAR_OUV listeParametreProto PAR_FER bloc {
          TypeData typeValue;
          typeValue.typeMethod = new MethodProperties($1,$5);
          Type type(TYPE_FONCTION,typeValue);
          Data data;
          data.ast = $7;
          $$ = new SymRecItem($3, type, data);
      }
    ;

listeParametreProto :
      listeParametreProto2 {$$ = $1;}
    | {$$ = new ParamsProtoList();}
    ;

listeParametreProto2 :
      parametreProto VIRG listeParametreProto {$$ = $3; $$->addParam($1);}
    | parametreProto {$$ = new ParamsProtoList(); $$->addParam($1);}
    ;

parametreProto :
      type IDENT {$$ = new ParamsProto($1,$2);}
    ;

typeAll :
      type {$$ = $1;}
    | TYPE_VOID {$$ = "void";}
    ;

type :
      TYPE_INT {$$ = (char*)"int";}
    | TYPE_FLOAT {$$ = (char*)"float";}
    | TYPE_BOOL {$$ = (char*)"bool";}
    | TYPE_COUL {$$ = (char*)"coul";}
    | IDENT {$$ = $1;}
    ;

bloc :
      ACCOL_OUV listeInstruction ACCOL_FER {$$ = $2;}
    ;

listeInstruction :
      allInstruction listeInstruction {$$ = $2; $$->addInstruction($1);}
    | {$$ = new InstructionList();}
    ;

allInstruction :
      blocControle {$$ = $1;}
    | instruction PV {$$ = $1;}
    ;

blocControle :
      MC_IF expression bloc {$$ = new ControleIf($2, $3);}
    | MC_IF expression bloc MC_ELSE bloc {$$ = new ControleIf($2, $3, $5);}
    | MC_FOR PAR_OUV forParametres PAR_FER bloc {$$ = new ControleFor($3, $5);}
    | MC_WHILE expression bloc {$$ = new ControleWhile($2, $3);}
    | MC_RETURN PV {$$ = new ControleReturn();}
    | MC_RETURN expression PV {$$ = new ControleReturn($2);}
    ;

instruction :
      primitive {$$ = $1;}
    | type IDENT {$$ = new VariableCreate($1, $2);}
    | IDENT AFFECT expression {$$ = new Affect($1, $3);}
    | type IDENT AFFECT expression {$$ = new VariableCreateAffect($1, $2, $4);}
    | objet PT IDENT AFFECT expression {$$ = new ObjectAttributeAffect($1, $3, $5);}
    | expression {$$ = $1;}
    ;

listeParametreAppel :
      listeParametreAppel2 {$$ = $1;}
    | {$$ = new ParamsList();}
    ;

listeParametreAppel2 :
      parametreAppel VIRG listeParametreAppel {$$ = $3; $$->addParam($1);}
    | parametreAppel {$$ = new ParamsList(); $$->addParam($1);}
    ;

parametreAppel :
      expression {$$ = $1;}
    ;


forParametres :
      listeForInstruction PV expression PV listeForInstruction {$$ = new ForParametres($1, $3, $5);}
    ;

listeForInstruction :
      instruction VIRG listeForInstruction {$$ = $3; $$->addInstruction($1);}
    | instruction {$$ = new InstructionList(); $$->addInstruction($1);}
    ;

objet :
      IDENT {$$ = $1;}
    | MC_THIS {$$ = (char*)"this";}
    ;

primitive :
      PRIM_AV PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimAvance($3);}
    | PRIM_RE PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimRecule($3);}
    | PRIM_TG PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimTourneG($3);}
    | PRIM_TD PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimTourneD($3);}
    | PRIM_LC PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimLever($3);}
    | PRIM_BC PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimBaisser($3);}
    | PRIM_SETCOUL PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimSetCoul($3);}
    | PRIM_SETPOS PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimSetPos($3);}
    | PRIM_SETANGL PAR_OUV listeParametreAppel PAR_FER {$$ = new PrimSetAngle($3);}
    ;

expression :
      expression OP_PLUS expression {$$ = new OperatorPlus($1,$3);}
    | expression OP_MOINS expression {$$ = new OperatorMoins($1,$3);}
    | expression OP_FOIS expression {$$ = new OperatorFois($1,$3);}
    | expression OP_DIV expression {$$ = new OperatorDiv($1,$3);}
    | expression OP_MOD expression {$$ = new OperatorMod($1,$3);}
    | expression OP_PUISS expression {$$ = new OperatorPuiss($1,$3);}
    | expression BOOL_ET expression {$$ = new BoolEt($1,$3);}
    | expression BOOL_OU expression {$$ = new BoolOu($1,$3);}
    | BOOL_NON expression {$$ = new BoolNon($2);}
    | PAR_OUV expression PAR_FER {$$ = $2;}
    | expression COMP_EQ expression {$$ = new CompEq($1,$3);}
    | expression COMP_DF expression {$$ = new CompDf($1,$3);}
    | expression COMP_PQ expression {$$ = new CompPq($1,$3);}
    | expression COMP_PE expression {$$ = new CompPe($1,$3);}
    | expression COMP_GQ expression {$$ = new CompGq($1,$3);}
    | expression COMP_GE expression {$$ = new CompGe($1,$3);}
    | ENTIER {$$ = new Entier($1);}
    | FLOT {$$ = new Flot($1);}
    | IDENT {$$ = new Ident($1);}
    | MC_FALSE {$$ = new Booleen($1);}
    | MC_TRUE {$$ = new Booleen($1);}
    | objet PT IDENT {$$ = new ObjectAttributeCall($1,$3);}
    | objet PT IDENT PAR_OUV listeParametreAppel PAR_FER {$$ = new ObjectMethodCall($1, $3, $5);}
    | IDENT PAR_OUV listeParametreAppel PAR_FER {$$ = new MethodCall($1, $3);}
    ;

%%
