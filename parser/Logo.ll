%{

/* precode */

#include "parser/Logo.tab.hh"
#include <string>
#include <iostream>
#include <cstring>
#include <cmath>
#include <queue>

#include "turtle/TurtleDebug.h"
#include "visitor/semanticCheck.h"
#include "visitor/EvalAst.h"

%}

%option yylineno

/* definition */

CHIFFRE [0-9]
LETTRE [A-Za-z]
COMMENT \/\/.*\n
IDENT {LETTRE}({LETTRE}|{CHIFFRE})*
ENTIER {CHIFFRE}+
FLOT {CHIFFRE}+\.{CHIFFRE}*
NOMFICHIER \"({LETTRE}|{CHIFFRE}|\/|\.|_)+\"

/* regles de production */

%%

{COMMENT} {}
"for" {return MC_FOR;}
"while" {return MC_WHILE;}
"if" {return MC_IF;}
"else" {return MC_ELSE;}
"class" {return MC_CLASS;}
"return" {return MC_RETURN;}
"include" {return MC_INCLUDE;}
"this" {return MC_THIS;}
"function" {return MC_FUNCTION;}
"true" {yylval.booleen = true; return MC_TRUE;}
"false" {yylval.booleen = false; return MC_FALSE;}

"{" {return ACCOL_OUV;}
"}" {return ACCOL_FER;}
"(" {return PAR_OUV;}
")" {return PAR_FER;}
"[" {return CROCH_OUV;}
"]" {return CROCH_FER;}
";" {return PV;}
"." {return PT;}
"," {return VIRG;}
":=" {return AFFECT;}

"+" {return OP_PLUS;}
"-" {return OP_MOINS;}
"*" {return OP_FOIS;}
"/" {return OP_DIV;}
"%" {return OP_MOD;}
"^" {return OP_PUISS;}

"==" {return COMP_EQ;}
"!=" {return COMP_DF;}
"<" {return COMP_PQ;}
"<=" {return COMP_PE;}
">" {return COMP_GQ;}
">=" {return COMP_GE;}
"&&" {return BOOL_ET;}
"||" {return BOOL_OU;}
"!" { return BOOL_NON;}

"int" { return TYPE_INT;}
"float" { return TYPE_FLOAT;}
"bool" {return TYPE_BOOL;}
"coul" {return TYPE_COUL;}
"void" {return TYPE_VOID;}

"avancer" {return PRIM_AV;}
"reculer" {return PRIM_RE;}
"gauche" {return PRIM_TG;}
"droite" {return PRIM_TD;}
"lever" {return PRIM_LC;}
"baisser" {return PRIM_BC;}
"setcouleur" {return PRIM_SETCOUL;}
"setpos" {return PRIM_SETPOS;}
"setangle" {return PRIM_SETANGL;}

{IDENT} {yylval.str = strdup(yytext); return IDENT;}
{ENTIER} {yylval.ent = atoi(yytext); return ENTIER;}
{FLOT} {yylval.flot = atof(yytext); return FLOT;}
{NOMFICHIER} {
    std::string str(strdup(yytext));
    std::string cleanedStr(str.substr(1,str.size()-2)); // Remove quotes
    yylval.str = strdup(cleanedStr.c_str()); return NOMFICHIER;}

[ \n\t] {}

%%
